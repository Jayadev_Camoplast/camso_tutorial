<!--**********************************************************************************
VF Page Name    : CS_OrderProductEntry
Description     : Product selection component for Order
Created By      : Reju Palathingal
Created Date    : 2014-07-15
Modification Log: 
---------------------------------------------------------------------------------- 
Developer                   Date                   Description
----------------------------------------------------------------------------------            
Reju Palathingal        15-Jul-2014            Initial Version
Divya                   25-Jul-2014            Add all fields according to wire frame
Divya                   30-Jul-2014            Location pick list displayed on the Page Block level
Pierre                  2014-08-01             Table Sort
Maryam                  2014-08-11             Updated discounts Sales Price
**********************************************************************************-->
<apex:page standardController="Order" extensions="CS_OrderProductsEntryExtn" action="{!priceBookCheck}" >

    <apex:sectionHeader Title="Manage {!$ObjectType.Product2.LabelPlural}" subtitle="{!order.Name}"/>
   <!-- <apex:messages style="color:red"/>-->

     <style>
        .search{
            font-size:14pt;
            margin-right: 20px;    
       
        .fyi{
            color:red;
            font-style:italic;
        }
        .label{
            margin-right:10px;
            font-weight:bold;
        }
        .total{
            margin-left:550px;
            margin-top:10px;
            font-weight:bold;
        }
        .totalweight{
            margin-left:100px;
            margin-top:10px;
            font-weight:bold;
                    }                    
        .freight{
            margin-left:10px;
            margin-top:10px;
            font-weight:bold;
                    }
         .freeweight{
            margin-left:826px;
            margin-top:10px;
            font-weight:bold;
                    }   
  </style>
    
    <script type='text/javascript'>
    
    
    
 
        // This script assists the search bar functionality
        // It will execute a search only after the user has stopped typing for more than 1 second
        // To raise the time between when the user stops typing and the search, edit the following variable:
        
        var waitTime = 1;
        
    
        var countDown = waitTime+1;
        var started = false;
        
        //Maryam Abbas function noenter - search to occur on hitting enter 
        function noenter(ev)  {

        if (window.event && window.event.keyCode == 13 || ev.which == 13) {

            fetchResults();
            
            //LMK 8/2/2014: Implemented to close autocomplete list upon pressing Enter.
            j$("input[id$=apexproductautocomplete]" ).autocomplete("close");
            return false;

         } else {

              return true;

         }

     }
        function resetTimer(){
        
            countDown=waitTime+1;
            
            if(started==false){
                started=true;
                runCountDown();
            }
        }
        
        function runCountDown(){
        
            countDown--;
            
            if(countDown<=0){
                fetchResults();
                started=false;
            }
            else{
                window.setTimeout(runCountDown,1000);
            }
        }
    
                
    </script>
    <!-- LMK 7222014 This code is for autocompleting the product names.-->    
    <script src="https://code.jquery.com/jquery-1.8.2.js"></script>
    <script src="/soap/ajax/26.0/connection.js" type="text/javascript"></script>
    <script src="https://code.jquery.com/ui/1.9.0/jquery-ui.js"></script>
    <script src="//cdn.datatables.net/1.10.2/js/jquery.dataTables.min.js"></script>
    
    <link rel="stylesheet" href="https://code.jquery.com/ui/1.9.1/themes/base/jquery-ui.css"/>
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.2/css/jquery.dataTables.css"/>

      <!--apex:includeScript value="{!URLFOR($Resource.JQueryUI, '/jquery-1.8.2.js')}"/>
    <apex:includeScript value="{!URLFOR($Resource.JQueryUI, '/jquery-ui.css')}"/>
    <apex:includeScript value="{!URLFOR($Resource.JQueryUI, '/jquery-ui.js')}"/-->  
    <style>
  .ui-autocomplete {
    max-height: 100px;
    overflow-y: auto;
    /* prevent horizontal scrollbar */
    overflow-x: hidden;
  }
  /* IE 6 doesn't support max-height
   * we use height instead, but this forces the menu to always be this tall
   */
  * html .ui-autocomplete {
    height: 100px;
  }
  </style>
      
       <script type="text/javascript">
       
    
        var j$ = jQuery.noConflict();
        var apexProductList =[];
       
        //use the <!-- <apex:repeat > -->tag to iterate through the list returned from the class and store only the names in the javascript variable.
        <apex:repeat value="{!getProductlist}" var="proList">     
       <!--apex:repeat value="{!AvailableProducts }" var="proList"-->
     
         //Store the name of the account in the array variable.
        apexProductList.push('{!proList.name}');
                        
        </apex:repeat>       
       
        
        //on Document ready
        j$(document).ready(function(){
             
            j$("input[id$=apexproductautocomplete]" ).autocomplete({
                source : apexProductList
            }).keydown(function(e){
    if (e.keyCode === 13){
        fetchResults();
    }
});

            InitTable();

        });

        function InitTable()
        {

             j$('[id*=ResultTable]').first().dataTable(
                {
                    "paging":   false,
                    "ordering": true,
                    "info":     false,
                    "searching": false,
                    "order": [[ 7, "desc" ]]
                }
            );
        }
 
    </script>

  
    <apex:form >
       <apex:actionFunction action="{!updatePriceAndWeight}" name="updatePriceAndWeight" rerender="selectedId">
        </apex:actionFunction>
        
        <apex:actionFunction action="{!applyDiscount}" name="applyDiscount" rerender="selectedId">
        </apex:actionFunction>    
        <apex:outputPanel id="mainBody">
        
            <apex:outputLabel styleClass="label">PriceBook: </apex:outputLabel>
            <apex:outputText value="{!theBook.Name}"/>&nbsp;
            <!--<apex:commandLink action="{!changePricebook}" value="change" immediate="true"/>-->
            <br/>
            <!-- not everyone is using multi-currency, so this section may or may not show -->
            <apex:outputPanel rendered="{!multipleCurrencies}">
                <apex:outputLabel styleClass="label">Currency: </apex:outputLabel>
                <apex:outputText value="{!chosenCurrency}"/>
                <br/>
            </apex:outputPanel>
            <br/>
            
            
<!-- this is the upper table... a.k.a. the "Shopping Cart"-->


            <!-- notice we use a lot of $ObjectType merge fields... I did that because if you have changed the labels of fields or objects it will reflect your own lingo -->
            <apex:pageBlock title="Selected {!$ObjectType.Product2.LabelPlural}" id="selectedId">
                <apex:pageMessages />
                       <apex:outputPanel >
                <apex:pageblockTable value="{!shoppingCart}" var="s" style="width:100%;">
                
                    <apex:column >
                        <apex:commandLink value="Remove" action="{!removeFromShoppingCart}" reRender="selectedId,searchResults" immediate="true">
                            <!-- this param is how we send an argument to the controller, so it knows which row we clicked 'remove' on -->
                            <apex:param value="{!s.PriceBookEntryId}" assignTo="{!toUnselect}" name="toUnselect"/>
                        </apex:commandLink>
                    </apex:column>
                    
                    
                   <apex:column headerValue="{!$ObjectType.Product2.Fields.ProductCode.Label}">
                        <apex:outputField value="{!s.PriceBookEntry.Product2.ProductCode}" style="width:70px "/>
                    </apex:column>
                     
                    <apex:column headerValue="{!$ObjectType.OrderItem.Fields.Back_Order__c.Label}">
                      <apex:inputField value="{!s.Back_Order__c}" required="false"/>
                    </apex:column>
                    
                    
                    <apex:column headerValue="{!$ObjectType.Product2.Fields.Family.Label}" rendered="false" >
                        <apex:outputField value="{!s.PriceBookEntry.Product2.Family}" style="width:70px"/>
                    </apex:column>
                    
                    
                    <apex:column headerValue="{!$ObjectType.Product2.Fields.Global_PID__c.Label}">
                        <apex:outputField value="{!s.PriceBookEntry.Product2.Global_PID__c}" style="width:70px "/>
                    </apex:column>
                    
                     <apex:column headerValue="{!$ObjectType.Product2.Fields.Location__c.Label}">
                        <apex:outputField value="{!s.PricebookEntry.Product2.Location__c}" style="width:70px" />
                    </apex:column> 
                      
                     <apex:column headerValue="{!$ObjectType.OrderItem.Fields.Quantity__c.Label}">
                        <apex:inputField value="{!s.Quantity__c}" style="width:50px" required="true" onkeyup="if(this.value!= '' && this.value !=null) this.value = ((isNaN(Math.floor(this.value)) )? null: Math.floor(this.value)); " onblur="if(this.value==0 || this.value =='' || this.value== null) this.value='1.00'; applyDiscount();"/>
                    </apex:column>
                    
                    
                   <apex:column headerValue="{!$ObjectType.OrderItem.Fields.ListPrice.Label}">
                   
                        <apex:outputField value="{!s.PricebookEntry.UnitPrice}" style="width:70px"/>
                    </apex:column>
                  
                     <apex:column headerValue="Price" style="width:160px" >
                             <apex:outputField value="{!s.PricebookEntry.CurrencyIsoCode}" style="width:30px" />
                             <apex:inputField value="{!s.UnitPrice}" style="width:70px" onkeyup="refreshTotals();"/>
                    </apex:column>
                    
                     <apex:column headerValue="Weight">
                        <apex:outputField value="{!s.PriceBookEntry.Product2.Weight__c}" style="width:70px"/>
                    </apex:column> 
                    <apex:column headerValue="{!$ObjectType.Product2.Fields.Unit_of_Measure__c.Label}">
                        <apex:outputField value="{!s.PriceBookEntry.Product2.Unit_of_Measure__c}" style="width:70px"/>
                    </apex:column> 
                   <apex:column headerValue="Vol. (m3)">
                        <apex:outputField value="{!s.PriceBookEntry.Product2.Volume__c}" style="width:70px"/>
                    </apex:column> 
                   
                    <apex:column headerValue="Wt Vol Ratio">
                        <apex:outputField value="{!s.Wt_Vol_Ratio__c}" style="width:120px"/>
                    </apex:column> 
                   
                   
                   <!-- Add Inventory Command Link--> 
                    
                    <apex:column headerValue="{!$ObjectType.OrderItem.Fields.Discount__c.Label}">
                        <apex:inputField value="{!s.Discount__c}"  style="width:50px" required="false"/>
                    </apex:column>
                    
                     
                    <apex:column headerValue="{!$ObjectType.OrderItem.Fields.Discount_Reason__c.Label}">
                        <apex:inputField value="{!s.Discount_Reason__c}" style="width:90px" required="false"/>
                    </apex:column>
                    
                    <apex:column headerValue="{!$ObjectType.OrderItem.Fields.Notes__c.Label}">
                        <apex:inputField value="{!s.Notes__c}" style="width:90px" required="false"/>
                    </apex:column>
                                     
                </apex:pageblockTable>
                </apex:outputPanel>
                       
                <apex:pageBlockButtons >
                    <apex:commandButton action="{!onSave}" value="Save"/>
                    <apex:commandButton action="{!onCancel}" value="Cancel" immediate="true"/>
                    <apex:commandButton action="{!applyDiscount}" value="Apply Discount" reRender="selectedId"/>
                 </apex:pageBlockButtons>
                   <br/>
                   <apex:outputLabel styleclass="total" value="Total Price :"/>
                    <!--<apex:outputLabel value="{!Totalsales}"/>-->
                    <apex:outputField value="{!theOrder.Total_Price__c}"/> 
                    <apex:outputLabel styleclass="totalweight" value="{!totalWeightLabel}"/>
                    <apex:outputLabel value="{!prodweight}"/> 
                    <apex:outputLabel styleclass="totalweight" value="Total Volume:"/>
                    <apex:outputLabel value="{!totalVolume}"/>   <br/>
                    
                    
                    <br/>
                    <apex:outputLabel styleclass="total" value="Added Discount % :"/>  
                    <apex:inputField value="{!theOrder.Added_Discount__c}" required="false"/>                   
                    <apex:outputLabel styleclass="freight" value="Free Freight" rendered="{!theOrder.Freight_Discount_Applied__c==true}" for="checkbox"/>
                    <apex:inputCheckbox id="checkbox" disabled="true" selected="true" rendered="{!theOrder.Freight_Discount_Applied__c==true}"/> <br/>
                    <apex:outputLabel styleclass="freeweight" value="{!weightLabel}" rendered="{!theOrder.Weight_Discount_Applied__c==true}" for="checkbox1"/>
                    <apex:inputCheckbox id="checkbox1" disabled="true" rendered="{!theOrder.Weight_Discount_Applied__c==true}" selected="true"/>
                    
             </apex:pageBlock>
             
    
<!-- this is the lower table: search bar and search results -->
    
              <apex:pageBlock >
            
                <apex:outputPanel styleClass="search">
                    Search for {!$ObjectType.Product2.LabelPlural}:
                </apex:outputPanel>

                <apex:actionRegion renderRegionOnly="false" >
                
                  <!-- commented to make the search bar to work with click of button -->
                    <apex:actionFunction name="fetchResults" action="{!updateAvailableList}" reRender="searchResults" status="searchStatus"/>

                    <apex:inputText id="apexproductautocomplete" value="{!searchString}" onkeypress="return noenter(event);" style="width:300px"/>

                    <!-- 7/21/2014 MFA - button added below for search -->
                    <apex:commandButton id="searchbutton" action="{!updateAvailableList}" reRender="searchResults" value="Search" status="searchStatus">
                        <apex:param value="true" assignTo="{!bGetDataFromExtSystem}" name="bGetDataFromExtSystem"/>
                    </apex:commandButton>
                    &nbsp;&nbsp;
                    <i>
                        <!-- actionStatus component makes it easy to let the user know when a search is underway -->
                        <!--apex:actionStatus id="searchStatus" startText="searching..." stopText=" "/-->
                        <apex:actionStatus id="searchStatus" onStop="InitTable();">
                            <apex:facet name="start" >
                                   <apex:image url="{!$Resource.ProgressBar}" width="150" />    
                            </apex:facet>
                            </apex:actionStatus>

                    </i>
                      </apex:actionRegion>
                 
                     <div align="right">
                <apex:outputLabel styleclass="Label" value="Location :"/> <apex:selectList value="{!ChosenValue}" multiselect="false" size="1">
                        <apex:selectOptions value="{!CountriesOptions}"/>
                </apex:selectList>
                </div>
            
                <br/>
                <br/>
               
             <apex:outputPanel id="searchResults">
                
                    <apex:pageBlockTable value="{!AvailableProducts}" var="a" id="ResultTable" styleclass="display datatable">
                    
                        <apex:column headerValue="{!$ObjectType.Product2.Fields.Name.Label}" value="{!a.Product2.Name}" />
                        
                        <apex:column headerValue="{!$ObjectType.Product2.Fields.Brand__c.Label}" value="{!a.Product2.Brand__c}" rendered="false"/>


                        <apex:column headerValue="{!$ObjectType.Product2.Fields.Product_Line__c.Label}" value="{!a.Product2.Product_Line__c}"/>


                        <apex:column headerValue="{!$ObjectType.Product2.Fields.ProductCode.Label}" value="{!a.Product2.ProductCode}"/>

                        <apex:column headerValue="{!$ObjectType.Product2.Fields.Global_PID__c.Label}" value="{!a.Product2.Global_PID__c}"/>
                        
                        <apex:column headerValue="{!$ObjectType.Product2.Fields.Description.Label}" value="{!a.Product2.Description}" rendered="false"/>
                        
                        <apex:column headerValue="{!$ObjectType.Product2.Fields.ABC_Classification__c.Label}" value="{!a.Product2.ABC_Classification__c}"/>
                        
                        <apex:column headerValue="Availability">
                            <apex:image id="theImage" value="{!If(a.Product2.ABC_Classification__c == 'G', 'https://cs18.salesforce.com/resource/1405641836000/Red_Notification','')}" rendered="{!If(a.Product2.ABC_Classification__c == 'G', 'true', 'false')}" width="16" height="16"/>
                        </apex:column>
                          <!-- Replacing Loaction picklist with a Text Field
                           <apex:column headerValue="Location" >
                                <apex:selectList value="{!ChosenValue}" rendered="{!(a.Product2.Family != '' && Contains(sValue,a.Product2.Family))}" multiselect="false" size="1">
                                <apex:selectOptions value="{!CountriesOptions}"/>
                            </apex:selectList>
                        </apex:column> -->
                         <apex:column headerValue="{!$ObjectType.Product2.Fields.Location__c.Label}" value="{!a.Product2.Location__c}"/>
                        
                        <apex:column headerValue="{!$ObjectType.Product2.Fields.On_hand__c.Label}" value="{!a.Product2.On_hand__c}"/>
                        
                        <apex:column headerValue="{!$ObjectType.Product2.Fields.Reserved__c.Label}" value="{!a.Product2.Reserved__c}"/>
                        
                        <apex:column headerValue="{!$ObjectType.Product2.Fields.Ordered__c.Label}" value="{!a.Product2.Ordered__c}"/>
                        
                        <apex:column headerValue="{!$ObjectType.Product2.Fields.Transit__c.Label}" value="{!a.Product2.Transit__c}"/>
                        
                        <apex:column headerValue="{!$ObjectType.Product2.Fields.AvailableInventory__c.Label}" value="{!a.Product2.AvailableInventory__c}"/>
                        
                        <apex:column headerValue="{!$ObjectType.PricebookEntry.Fields.UnitPrice.Label}" value="{!a.UnitPrice}"/>
                        <apex:column headerValue="Details">
                            <a href="/{!a.Product2Id}" target= "_blank">Details</a>
                            <!-- <apex:commandLink value="Details" onclick="window.open('/{a.Product2Id');" style="align:right;" reRender="selectedId,searchResults" immediate="true"/> -->
                        </apex:column>https://c.cs18.visual.force.com/s.gif
                                               
                        <apex:column >
                            <!-- command button in a column... neato -->
                            <apex:commandButton value="Select" action="{!addToShoppingCart}" reRender="selectedId,searchResults,addedsearchResults" immediate="true">
                                <!-- again we use apex:param to be able to tell the controller which row we are working with -->
                                <apex:param value="{!a.Id}" assignTo="{!toSelect}" name="toSelect"/>
                            </apex:commandButton>
                        </apex:column>
                        
                    </apex:pageBlockTable>
                    <!-- We put up a warning if results exceed 100 rows -->
                    <apex:outputPanel styleClass="fyi" rendered="{!overLimit}">
                        <br/>
                        Your search returned over 100 results, use a more specific search string if you do not see the desired {!$ObjectType.Product2.Label}.
                        <br/>
                    </apex:outputPanel>
                    
                </apex:outputPanel>
            
            </apex:pageBlock>
            
        </apex:outputPanel>

    </apex:form>

</apex:page>