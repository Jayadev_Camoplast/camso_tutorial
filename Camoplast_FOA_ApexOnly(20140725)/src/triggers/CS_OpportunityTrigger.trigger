/*************************************************************************************
Trigger Name : CS_OpportunityTrigger
Description : Trigger for ‘Opportunity' object. 
Created By : Maryam Abbas
Created Date : 14-Jul-14
Modification Log:
---------------------------------------------------------------------------------- 
Developer Date Description
---------------------------------------------------------------------------------- 
Maryam 14-Jul-14 Initial Version
************************************************************************************/
trigger CS_OpportunityTrigger on Opportunity(before insert,before update,after insert) {

    if(Trigger.isBefore && (Trigger.isInsert ||Trigger.isUpdate)){
        CS_Trigger_Opportunity.updateOptyLocationFields(trigger.New);
        CS_Trigger_Opportunity.updateOpportunityPriceBook(trigger.new);
        CS_Opportunity_Update.updateQuotepricebook(trigger.new);
       // CS_Opportunity_Update.updateOpportunityBilltoContact(trigger.new);
       //CS_Opportunity_Update.updateOrderShiptoAcc(trigger.new);
        CS_Trigger_Opportunity.updateBillToShipToFields(trigger.new);
        CS_Trigger_Opportunity.updateAccountInfo(trigger.new);
        
    }
  
    if(Trigger.isAfter && Trigger.isInsert){
        CS_Trigger_Opportunity.updateTermsandConditions(trigger.new);
        
        
    }
}