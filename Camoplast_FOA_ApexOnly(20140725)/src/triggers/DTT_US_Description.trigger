// Pierre Dufour - 2014-07-11 - Automactically set the description if US.RecordTYpe is set to Functional Requirement.
trigger DTT_US_Description on User_Story__c (before insert, before update) {

	map<Id, RecordType> recordMap = new map<Id, RecordType>([Select Id, Name from RecordType]);
	
	for (User_Story__c u : trigger.new)
	{
		if (u.RecordTypeId != null)
		{
			if (recordMap.get(u.RecordTypeId).Name == 'Functional Requirement')
			{
				u.Description__c = string.format('As a {0} I want to {1} so that {2}', new string[]{u.As_A__c, u.I_Want_To__c, u.So_that__c});

			}
		}
	}

}