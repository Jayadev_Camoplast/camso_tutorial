/*************************************************************************************
Trigger Name : CS_Approval_Status_Update
Description : Trigger for 'Opportunity Product' object. 
Created By : Laxmy M Krishnan
Created Date : 15-Jul-14
Modification Log:
---------------------------------------------------------------------------------- 
Developer Date Description
---------------------------------------------------------------------------------- 
Laxmy 15-Jul-14 Initial Version
************************************************************************************/
trigger CS_Approval_Status_Update on OpportunityLineItem (before update,before insert) {

    if((Trigger.isBefore && Trigger.isInsert) || (Trigger.isBefore && Trigger.isUpdate)){
        CS_Opportunity_Line_Item_Update.updateApprovalStatusOppoLi(trigger.new);
    }
}