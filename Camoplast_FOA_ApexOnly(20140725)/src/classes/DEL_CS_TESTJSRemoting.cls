Global Class DEL_CS_TESTJSRemoting {

//    Public String sPriceBookId{get;set;}
    
    @RemoteAction
    Global Static List<String> getProductNames(String sPriceBookId) {
        // Get the PriceBook Id from the record
        ID PBId = '01sb0000001kBIt';//=string.ValueOf(oCommonObject.get('Pricebook2Id'));
        
        // Query for all the products in that Price Book:
        List<String> lstProductNames = new List<String>();
        For(PriceBookEntry pbe: [SELECT Id, Product2.Name FROM PriceBookEntry WHERE PriceBook2Id = :sPriceBookId]){
            lstProductNames.add(pbe.Product2.Name);
        }
        return lstProductNames;
    }


}