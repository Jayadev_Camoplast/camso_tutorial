/****************************************************************************************************************************************** 
* Class Name   l DTT_UpdaterOwner.cls
* Description  : Batch update any object 
* Created By   : Deloitte Consulting - Pierre Dufour - pidufour@deloitte.ca
* 
*****************************************************************************************************************************************/
global class DTT_UpdaterOwner  implements Database.Batchable<sObject> {


        private string m_Obj;
        private string m_Filter = 'Id != null';
        private string m_ResetField = null;
        private string m_ReassignField = null;
        private string m_ResetOwnership = null;
        private map<string, User> m_UserMap = new map<string, User>();

        global DTT_UpdaterOwner(string ObjectName){
                m_Obj = ObjectName;
        }

        global DTT_UpdaterOwner(string ObjectName, string filter){
                m_Obj = ObjectName;
                m_Filter = filter;
        }

        global DTT_UpdaterOwner(string ObjectName, string filter, string resetField){
                m_Obj = ObjectName;
                m_Filter = filter;
                m_ResetField = resetField;
        }

        global DTT_UpdaterOwner(string ObjectName, string filter, string resetField, string reassignField){
                m_Obj = ObjectName;
                m_Filter = filter;
                m_ResetField = resetField;
                m_ReassignField  = reassignField;

                if (m_ReassignField != null)
                        for (USer u : [Select Id,  Legacy_Name__c from User where IsActive = true and Legacy_Name__c != null])
                        	m_UserMap.put(u.Legacy_Name__c.toLowerCase(), u);

        }

        global DTT_UpdaterOwner(string ObjectName, boolean ResetFlag, string ResetOwnership){
                m_Obj = ObjectName;
                m_ResetOwnership = ResetOwnership;
        }

        global Database.QueryLocator start(Database.BatchableContext bc) {
                String query = 'Select Id, OwnerId from ' +m_Obj + ' where ' + m_Filter;
                
                if (m_ReassignField != null)
                        query = 'Select Id, OwnerId,' + m_ReassignField + ' from ' +m_Obj + ' where ' + m_Filter;
                        

                return Database.getQueryLocator(query);
        }

        global void execute(Database.BatchableContext BC, list<Sobject> scope){

                if (m_ResetField != null)
                        for (Sobject s : scope)
                                s.put(m_Resetfield, null);

                if (m_ResetOwnership != null)
                	for (Sobject s : scope)
                    	s.put('OwnerId', m_ResetOwnership);

                if (m_ReassignField != null)
					for (Sobject s : scope)
                    	Reassign(s);

                Database.update(scope,false);

        }

        public void Reassign(sObject tempObject)
        {
        	string legacyUser = tempObject.get(m_ReassignField) == null ? null : string.valueof(tempObject.get(m_ReassignField));
        	
        	if (legacyUser != null)
        		if (legacyUser != '')
        			if (m_UserMap.containsKey(legacyUser.toLowerCase()))
        				tempObject.put('OwnerId', m_UserMap.get(legacyUser.toLowerCase()).Id);
        }

        
        global void finish(Database.BatchableContext BC){



        }



}