// Check for all validations before navigating to the order creation page:
// HOW TO COVER WHEN DOING : Quote to ORDER??
// Use Custom Labels.
Public Class CS_NewOrderExtension{
    
    Private Final String sACCOUNT_CREDIT_STATUS = 'ACTIVE';
    Public String sAccountId {get;set;}
    
    Public CS_NewOrderExtension(ApexPages.StandardController controller) {
        // Get the account details first:
        // If from an Account page:
        sAccountId = ApexPages.CurrentPage().getParameters().get('aid');
        

    }
    
    Public PageReference Redirect() {
        // Check if this is an account Id or not (needs to be generailised)
        if(sAccountId != null && sAccountId != '' && sAccountId.StartsWith('001')) {
            // Collect the Account details from account Id:
            List<Account> lstAccount = [SELECT Id, Credit_Status__c FROM Account WHERE Id = :sAccountId.left(15)];
            if(lstAccount != null) {
                
                // Check if the credit status is Active or not. If not active don't proceed
                if(lstAccount[0].Credit_Status__c != sACCOUNT_CREDIT_STATUS) {
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, 'Can not create new Order. Account Credit Status is : '+ lstAccount[0].Credit_Status__c));
                    return null;
                }
                // More validation to be added here:
                else if(false) {
                
                }
                // Any other validations
                else {
                }
            }
        }
        
        // If all validtions passed, Redirect to new Order creation page:
        PageReference oNewOrderPage = new PageReference('/801/e?nooverride=1');
        for(String s: ApexPages.CurrentPage().getParameters().keySet()){
            // Skip the 'save_new' parameter as it is causing a invalid session error:
            if(s != 'save_new')
                oNewOrderPage.getParameters().put(s, ApexPages.CurrentPage().getParameters().get(s));
        }
        return oNewOrderPage;

    }
    
    // Go back to Account screen
    Public PageReference Cancel() {
        return new PageReference('/'+sAccountId);
    }
}