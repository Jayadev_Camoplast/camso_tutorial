/*********************************************************************************
Class Name      : CS_TestClass_Order
Description     : This class is used for Test Classes for Functionalities on Order object
Created By      : Maryam Abbas
Created Date    : 15-Jul-14
Modification Log:
---------------------------------------------------------------------------------- 
Developer                   Date                   Description
----------------------------------------------------------------------------------            
Maryam Abbas              15-Jul-14               Initial Version
Divya A N                 16-Jul-14               updateOrderFields Method
Divya A N                 09-Sep-2014             updated test_updateOrderFields() method for Ship To Account to look up to the Junction Object
*********************************************************************************/
@isTest
public class CS_Test_Order{
 /*********************************************************************************
 Method Name    : test_updateOrderPriceBook
 Description    : Test method to update price book based on account selection on order
 Return Type    : void
 Parameter      : nil                 
 *********************************************************************************/
    
       static testmethod void test_updateOrderPriceBook(){
             
             Pricebook2 pb=new Pricebook2();
                 pb.Name= 'Test';
                 pb.Construction_Discount_Class__c= 'C';
                 pb.MH_Discount_Class__c= 'B';
             insert pb;
             Pricebook2 pb1= new Pricebook2();
                 pb1.Name= 'Test2'; 
                 pb1.Construction_Discount_Class__c= 'B';
                 pb1.MH_Discount_Class__c= 'B';
             insert pb1;
            /* Pricebook2 pbstd= new Pricebook2();
                 pbstd.Name= 'Test2'; 
                 pbstd.Construction_Discount_Class__c= 'B';
                 pbstd.MH_Discount_Class__c= 'B';
                 pbstd.IsStandard = true;
             insert pbstd;*/
             
             datetime myDateTime = datetime.now();
             test.starttest();     
             Account acc= new Account();
                 acc.Name = 'Test Account';
                 acc.CurrencyIsoCode = 'EUR';            
                 acc.Con_Discount_Class__c = 'C';
                 acc.MH_Discount_Class__c =  'B';                 
             insert acc;
             Terms_and_Conditions__C terms = new Terms_and_Conditions__c();
             terms.Terms_and_Conditions__c = 'New Term';
             terms.AccountId__c = acc.ID;
             insert terms;
            // RecordType rec = new RecordType();
            // rec.Name = 'North America';
             RecordType rec = [SELECT Id FROM RecordType where Name = 'North America' limit 1];
             
             Account accnt = [SELECT Assigned_Price_Book__c FROM Account WHERE ID = :acc.ID limit 1];    
             accnt.Assigned_Price_Book__c= pb.ID;  
             Order ord = new Order();
                 ord.PoNumber= '123';
                 ord.EffectiveDate = myDateTime.date() ;            
                 ord.RecordType= rec;
                 ord.AccountId =  acc.ID;   
                 ord.Status = 'Draft';              
             insert ord;
             Order order = [SELECT PriceBook2Id FROM Order WHERE ID=:ord.ID limit 1];
             Terms_and_Conditions__c TC = [SELECT Terms_and_Conditions__c,OrderId__c FROM Terms_and_Conditions__c WHERE OrderId__c =:ord.ID limit 1];
             system.Assert(order.PriceBook2Id == pb.ID);  
             system.Assert(TC.OrderId__c!=null); 
         /*    Account acc1= new Account();
                 acc1.Name = 'Test Account1';
                    
             insert acc1;
               system.Assert(acc1.Id != null);
             Opportunity opp1= new Opportunity();
                 opp1.Name = 'Test Opty 2';
                 opp1.CloseDate = myDateTime.date();            
                 opp1.StageName = 'Qualification';
                 opp1.AccountId = acc1.ID;                 
             insert opp1;
             Opportunity opty1 = [SELECT PriceBookId__c FROM Opportunity WHERE ID =:opp1.ID limit 1];
             system.Assert(opty1.PriceBookId__c== pb.ID); 
             List<Pricebook2> cs_DefaultPrice = [Select Id FROM Pricebook2 WHERE Pricebook2.IsStandard = true];
             system.Assert(cs_DefaultPrice.get(0).Id!=null);
             system.Assert(opty1.PriceBookId__c=='01sb0000001kBItAAM');  */
        test.stoptest();              
      }
      
  /*********************************************************************************
 Method Name    : test_updateOrderFields
 Description    : Test method to update Order Fields based on Solt To/Ship To Accounts
 Return Type    : void
 Parameter      : nil                 
 *********************************************************************************/     

    static testMethod void test_updateOrderFields(){
    
    Pricebook2 pb=new Pricebook2();
                 pb.Name= 'ABCDE';
                 pb.Construction_Discount_Class__c= 'C';
                 pb.MH_Discount_Class__c= 'B';
                 
             insert pb;
             
     Account oAcc= new Account();
                 oAcc.Name = 'Test Account';
                 oAcc.CurrencyIsoCode = 'USD'; 
                 oAcc.BillingCity= 'Alabama';
                 oAcc.BillingCountry= 'USA';
                 oAcc.BillingStreet= 'A';
                 oAcc.BillingState= 'Test';
                 oAcc.BillingPostalCode= '12345';
                 oAcc.Con_Discount_Class__c = 'C';
                 oAcc.MH_Discount_Class__c =  'B';   
               insert oAcc;
               
     Account oAcc1= new Account();
                 oAcc1.Name = 'Test Account2';
                 oAcc1.CurrencyIsoCode = 'USD';
                 //acc1.ShippingAddress='A,Test1,Arizona,123456,USA'; 
                 oAcc1.ShippingCity= 'ABCD';
                 oAcc1.ShippingCountry= 'USA';
                 oAcc1.ShippingStreet= 'C';
                 oAcc1.ShippingState= 'Arizona';
                 oAcc1.ShippingPostalCode= '123455';
                 oAcc1.ship_To__c= True;
                 oAcc1.ParentId = oAcc.Id;
                 oAcc1.Con_Discount_Class__c = 'C';
                 oAcc1.MH_Discount_Class__c =  'B';   
               insert oAcc1;
    RecordType rec = [SELECT Id FROM RecordType where Name = 'North America']; 
     datetime myDateTime = datetime.now();          
    Order oOrd = new Order();
                 oOrd.PoNumber= '123';
                 oOrd.EffectiveDate = myDateTime.date() ;            
                 oOrd.RecordType= rec;
                 oOrd.AccountId =  oAcc.ID;   
                 oOrd.Status = 'Draft';              
             insert oOrd;
             
     Order oOrd1= [SELECT Sold_To_Address_Cont__c,Location__c,Tax__c from Order where ID =: oOrd.ID];
     Account soldTo=[SELECT BillingCity,BillingCountry,BillingStreet,BillingState,BillingPostalCode from Account where ID= :oAcc.ID];
     system.assert(oOrd1.Sold_To_Address_Cont__c == soldTo.BillingCity + ', ' + soldTo.BillingState + ', ' + soldTo.BillingCountry + ', ' + soldTo.BillingPostalCode);
     system.assert (oOrd1.Location_old__c=='ZSNA');
             oOrd1.Ship_To_Account_RelatedAccount__r.Ship_To__c = oAcc1.ID;
             //Divya 9/9: Ship To Account on Ord will now look up to the Junction Object Related Accounts
             //oOrd1.Ship_To_Account__c= oAcc1.ID;
             update oOrd1;
     oOrd1= [SELECT Sold_To_Address_Cont__c,Ship_To_Address_cont__c,Location__c,Tax__c from Order where ID =: oOrd.ID];
     Account shipTo=[SELECT ShippingCity,ShippingCountry,ShippingStreet,ShippingState,ShippingPostalCode from Account where ID=:oAcc1.ID];
      system.assertEquals(oOrd1.Ship_To_Address_cont__c, shipTo.ShippingCity + ', ' + shipTo.ShippingState + ', ' + shipTo.ShippingCountry + ', ' + shipTo.ShippingPostalCode ,'Shipping Address not populated properly');

           soldTo.LOC__c='1CHA';
           update soldTo;
      soldTo=[SELECT BillingCity,BillingCountry,BillingStreet,BillingState,BillingPostalCode,LOC__c from Account where ID=:oAcc.ID];
            oOrd1.PoNumber= '457';
            update oOrd1;
     oOrd1= [SELECT Location_old__c,Tax__c from Order where ID =: oOrd.ID];
     system.assert(soldTo.LOC__c==oOrd1.Location__c);
             shipTo.LOC__c= '1JOL'; 
             shipTo.Tax_Exemption__c= True;
             update shipTo;
     shipTo=[SELECT ShippingCity,ShippingCountry,ShippingStreet,ShippingState,ShippingPostalCode,LOC__c,Tax_Exemption__c from Account where ID=:oAcc1.ID];
             oOrd1.PoNumber= '789';
             update oOrd1;
     oOrd1= [SELECT Location_old__c,Tax__c from Order where ID =: oOrd.ID];
     
     system.assert (oOrd1.Location__c==shipTo.LOC__c);
     system.assert (oOrd1.Tax__c== '0');
      
    }      
    
    
/*********************************************************************************
Method Name : test_updateOrderBilltoContact
Description : To update the Bill to Contact with the Account's primary Contact with Role as Orders Main Contact and
update Ship to Account based on Ship to Contact.
Return Type : void
Parameter : nill
Author: Laxmy M Krishnan
Date : 7/31/2014
*********************************************************************************/

 static testMethod void testclass_updateOrderFields(){
    
    Pricebook2 pb=new Pricebook2();
                 pb.Name= 'ABCDE';
                 pb.Construction_Discount_Class__c= 'C';
                 pb.MH_Discount_Class__c= 'B';
                 
             insert pb;
             
    /* Account oAcc= new Account();
                 oAcc.Name = 'Test Update Account';
                 oAcc.CurrencyIsoCode = 'USD'; 
                 oAcc.BillingCity= 'Alabama';
                 oAcc.BillingCountry= 'United States';
                 oAcc.BillingStreet= 'A';
                 oAcc.BillingState= 'Test';
                 oAcc.BillingPostalCode= '12345';
                 oAcc.Con_Discount_Class__c = 'C';
                 oAcc.MH_Discount_Class__c =  'B';   
               insert oAcc;
*/
                   
     Account oAcc1= new Account();
                 oAcc1.Name = 'Test Account2';
                 oAcc1.CurrencyIsoCode = 'USD';
                 //acc1.ShippingAddress='A,Test1,Arizona,123456,USA'; 
                 //oAcc1.ShippingCity= 'ABCD';
                 //oAcc1.ShippingCountry= 'USA';
                 //oAcc1.ShippingStreet= 'C';
                 //oAcc1.ShippingState= 'Alabama';
                 //oAcc1.ShippingPostalCode= '123455';
                 oAcc1.ship_To__c= True;
                 //oAcc1.ParentId = oAcc.Id;
                 oAcc1.Con_Discount_Class__c = 'C';
                 oAcc1.MH_Discount_Class__c =  'B';   
               insert oAcc1;


Contact oCon = new Contact();
    oCon.LastName='TestClass';
    oCon.AccountId=oAcc1.ID;
    insert oCon;

    RecordType rec = [SELECT Id FROM RecordType where Name = 'North America']; 
     datetime myDateTime = datetime.now();          
    Order oOrd = new Order();
                 oOrd.PoNumber= '123';
                 oOrd.EffectiveDate = myDateTime.date() ;            
                 oOrd.RecordType= rec;
                 oOrd.AccountId =  oAcc1.ID;   
                 oOrd.Status = 'Draft';              
             insert oOrd;

system.assert (oOrd.Id!=null);

}         
}