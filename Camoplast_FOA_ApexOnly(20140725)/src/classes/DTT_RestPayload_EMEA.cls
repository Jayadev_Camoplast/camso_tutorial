public  class DTT_RestPayload_EMEA {
	
	Public Class ProductInformation{
        
        public String SessionId {get;set;}
        public String ServerUrl {get;set;}
        
        public string DistrChan {get; set;}
	    public string Division {get; set;}
	    public string Material {get; set;}
	    public string Plant {get; set;}
	    public string SoldTo {get; set;}
    }
}