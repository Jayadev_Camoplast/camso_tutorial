/*********************************************************************************
Class Name      : CS_Quote_PostControl
Description     : This is class is the extension for CS_QuoteStatusBanner
Created By      : Divya A N
Created Date    : 2014-09-11
Modification Log:
---------------------------------------------------------------------------------- 
Developer                   Date                   Description
----------------------------------------------------------------------------------            
Divya A N                 11-09-2014            Initial Version
*********************************************************************************/
public with sharing class CS_Quote_PostControl {


    private Quote refQuote = null;
    public Boolean refreshPage {get; set;}
   
    public CS_Quote_PostControl(ApexPages.StandardController controller) {

        if (controller != null)
            if (controller.getRecord() != null)
                    for (Quote oQuote : [Select Id, Posted__c,Last_Sync_Datetime_with_ERP__c,lastmodifieddate,Is_Synced_From_SFDC__c  from Quote where Id =: controller.getRecord().Id limit 1]) 
                        refQuote = oQuote;
    }

    public PageReference Post()
    {
        refQuote.Posted__c = true;
        refQuote.Last_Sync_Datetime_with_ERP__c  = system.now();
        update refQuote;
        refreshPage = true;
        return null;
    }
    
    public void Refresh()
    {

            if(refQuote.Posted__c  && (refQuote.Last_Sync_Datetime_with_ERP__c <= refQuote.lastModifiedDate)){
            refQuote.Last_Sync_Datetime_with_ERP__c  = system.now();
             update refQuote;
             }
        refreshPage = true;
        
    }
 }