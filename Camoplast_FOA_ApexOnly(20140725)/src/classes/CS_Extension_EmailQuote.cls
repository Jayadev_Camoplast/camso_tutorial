/*************************************************************************************
Classe Name : CS_Extension_EmailQuote
Description : Class to generate quote and PDF on mobile.
Created By  : Jayadev Rath
Created Date: 14-July-14
Modification Log:
---------------------------------------------------------------------------------- 
Developer             Date             Description
---------------------------------------------------------------------------------- 
Jayadev Rath         14-Jul-2014       Initial Version
************************************************************************************/
Public class CS_Extension_EmailQuote{

    Private Opportunity oOpp{get;set;}
    Public String sErrorFound {get;set;}
    Private ApexPages.StandardController con{Get;set;}

    /*********************************************************************************
    Method Name    : Constructor
    Description    : Verify if the any approval is required on Opportunity. If yes, then restrict generating a quote & sending email
    Return Type    : N/A
    Parameter      : None
    *********************************************************************************/
    public CS_Extension_EmailQuote(ApexPages.StandardController controller) {
        //system.debug('$$'+ URL.getCurrentRequestUrl() + '$$' + URL.getSalesforceBaseUrl() );
        system.debug('$$'+ System.URL.getSalesforceBaseURL().getHost());
        con = controller;
        sErrorFound = 'No';
        oOpp = [SELECT Id,Name,AccountId, SyncedQuoteId, Pricebook2Id,Approval_Status__c,Quote_Status__c,Expiration_Date__c,Reason_for_Rejection__c,Description,
                Quote_Prepared_For__c,Quote_Prepared_For__r.Phone, Quote_Prepared_For__r.Fax,Quote_Prepared_For__r.Email,Ship_To_Contact__c,
                (SELECT Id,Description,Discount,PricebookEntryId,Quantity,UnitPrice FROM OpportunityLineItems)
                FROM Opportunity WHERE Id = :controller.getId() ];
                
        if(oOpp.Approval_Status__c == 'Required') {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,'Opportunity should be approved to Send a Quote via email'));
            sErrorFound = 'Yes';    // Used for making navigation decision in Salesforce 1
            return;
        }
    }
    /*    
    // Create a Quote record and redirect to Opportunity Page
    Public PageReference createQuote(){
    
        oOpp = [SELECT Id,Name,AccountId,Pricebook2Id,Approval_Status__c,Quote_Status__c,Expiration_Date__c,Reason_for_Rejection__c,Description,
                Quote_Prepared_For__c,Quote_Prepared_For__r.Phone, Quote_Prepared_For__r.Fax,Quote_Prepared_For__r.Email,Ship_To_Contact__c,
                (SELECT Id,Description,Discount,PricebookEntryId,Quantity,UnitPrice FROM OpportunityLineItems)
                FROM Opportunity WHERE Id = :con.getId() ];

        // Create a Quote record from Opportunity:
        Quote oQuoteRecord = new Quote(Name= oOpp.Name + '- Quote',OpportunityId=oOpp.Id, Pricebook2Id=oOpp.Pricebook2Id, Status=oOpp.Quote_Status__c,
                                        ExpirationDate=oOpp.Expiration_Date__c, Reason_for_Rejection__c=oOpp.Reason_for_Rejection__c, Description=oOpp.Description,
                                        Ship_To_Contact__c = oOpp.Ship_To_Contact__c, ContactId=oOpp.Quote_Prepared_For__c, Phone=oOpp.Quote_Prepared_For__r.Phone, 
                                        Email=oOpp.Quote_Prepared_For__r.Email, Fax=oOpp.Quote_Prepared_For__r.Fax);
        insert oQuoteRecord;
        
        // Get Fresh Values from DB:
        oQuoteRecord = [SELECT Id,Name,ContactId FROM Quote WHERE Id =: oQuoteRecord.Id];
        
        // Create new QuoteLineItems:
        system.debug('%%'+'after quote creation: '+oOpp.OpportunityLineItems); 
        if(oOpp.OpportunityLineItems!=null && oOpp.OpportunityLineItems.size()>0){
            List<QuoteLineItem> lstQLI = new List<QuoteLineItem>();
            for(OpportunityLineItem oli : oOpp.OpportunityLineItems){
                QuoteLineItem qli = new QuoteLineItem();
                qli.QuoteId = oQuoteRecord.Id;
                qli.Description = oli.Description;
                qli.Discount = oli.Discount;
                qli.PricebookEntryId = oli.PricebookEntryId;
                qli.Quantity = oli.Quantity;
                qli.UnitPrice = oli.UnitPrice;
                lstQLI.add(qli);
            }
            insert lstQLI;
        }
        return null;
        return returnToOpportunity();
    }
    */

    /*********************************************************************************
    Method Name    : emailQuote
    Description    : Method to Email quote record using Conga merge
    Return Type    : PageReference 
    Parameter      : None
    *********************************************************************************/
    Public PageReference emailQuote(){

        // Pierre Dufour - Let's work with the synced quote Id.
        list<Quote> lstQuote = [Select Id,Name,ContactId from Quote Where OpportunityId = :con.getId() ORDER BY CreatedDate desc LIMIT 1];
        if(lstQuote==null || lstQuote.size()==0) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,'No Quote available for this Opportunity'));
            sErrorFound = 'Yes';    // Used for making navigation decision in Salesforce 1
            return null;
        }
        Quote oQuote = lstQuote[0];
        
        if(oQuote!=null) {
            try {
                String EMAIL_TEMPLATE_NAME='New_Quote',CONGA_QUERY_NAME='New Quote',CONGA_TEMPLATE_NAME='New Quote';
                String sQuoteId, sQuoteName,sCongaQueryId,sCongaTemplateId,sEmailTemplateId;
    
                // Get the Ids for preparing the Conga Outbound URL:
                sQuoteId =  String.valueOf(oQuote.Id).left(15);
                sQuoteName = oQuote.Name;
                String sQuoteContactId = String.valueOf(oQuote.ContactId).left(15);
                
                // Get the Query ID:
                List<APXTConga4__Conga_Merge_Query__c> lstCongaQueries = [SELECT Id FROM APXTConga4__Conga_Merge_Query__c WHERE APXTConga4__Name__c= :CONGA_QUERY_NAME limit 1];
                if(lstCongaQueries!=null && lstCongaQueries.size()>0)
                    sCongaQueryId = String.valueOf(lstCongaQueries[0].Id).left(15);
                    
                // Get the 'New Quote' Template Id:
                List<APXTConga4__Conga_Template__c> lstCongaTemplates = [SELECT Id FROM APXTConga4__Conga_Template__c WHERE APXTConga4__Name__c = :CONGA_TEMPLATE_NAME limit 1];
                if(lstCongaTemplates!=null && lstCongaTemplates.size()>0)
                    sCongaTemplateId = String.valueOf(lstCongaTemplates[0].Id).left(15);
                    
                // Get the Email Template Details:
                List<EmailTemplate> templates = [SELECT Id,Name FROM Emailtemplate WHERE DeveloperName = :EMAIL_TEMPLATE_NAME AND isActive=true LIMIT 1];
                //System.assert(!(templates == null || templates.size()==0),'No Email Templates found while sending Conga outbound email');
                if(templates!=null && templates.size()>0)
                    sEmailTemplateId = String.valueOf(templates[0].Id).left(15);
                
                // Make a outbound call to Conga:
                String sEndPointUrl = 'https://composer.congamerge.com/composer8/index.html';
                
                sEndPointUrl += '?sessionId=' + UserInfo.GetSessionId();
                sEndPointUrl += '&serverUrl='+ URL.getSalesforceBaseUrl().toExternalForm() + '/services/Soap/u/28.0/' + String.valueOf(UserInfo.getOrganizationId()).left(15);
                sEndPointUrl += '&Id='+ sQuoteId;
                
                sEndPointUrl += '&TemplateId='+ sCongaTemplateId;
                sEndPointUrl += '&EmailTemplateId=' + sEmailTemplateId;
                sEndPointUrl += '&EmailToId=' + sQuoteContactId;
                sEndPointUrl += '&EmailAdditionalTo=jarath@deloitte.com';
                sEndPointUrl += '&QueryId=a1311000000LAzz,' + sCongaQueryId +  '?pv0=' + sQuoteId ;
                sEndPointUrl += '&DefaultPDF=1&DS7=12&APIMode=12';
                
                // Call future callout method :
                makeCallout(sEndPointUrl);
            } Catch(exception e) {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,e.getMessage()));
                sErrorFound = 'Yes';    // Used for making navigation decision in Salesforce 1
                //return null;
            }
        }
        return null;
        return returnToOpportunity();
    }
    
    /*********************************************************************************
    Method Name    : makeCallout
    Description    : Future method for calling out to Conga
    Return Type    : void
    Parameter      : 1. CongaURL: The url which is used to make the HTTP Callout
    *********************************************************************************/
    @Future(callout=true)
    public static void makeCallout(String congaURL){
            Http oHttp = new Http();          
            HttpRequest oHttpReq = new HttpRequest();            
            oHttpReq.setEndpoint(congaURL);
            oHttpReq.setMethod('GET');
            oHttpReq.setTimeout(60000);
            HttpResponse oHttpRes;
            oHttpRes = oHttp.send(oHttpReq);
            system.debug('%% '+oHttpRes);
    }
    
    /*********************************************************************************
    Method Name    : returnToOpportunity
    Description    : Redirects to the parent opportunity page
    Return Type    : PageReference 
    Parameter      : None
    *********************************************************************************/
    Public PageReference returnToOpportunity(){
        PageReference oRedirectPage = new PageReference('/'+oOpp.Id); 
        oRedirectPage.setRedirect(true);
        return oRedirectPage;
    }
}