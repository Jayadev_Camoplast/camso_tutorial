/****************************************************************************************************************************************** 
* Class Name   : DTT_Updater.cls
* Description  : Batch update any object 
* Created By   : Deloitte Consulting - Pierre Dufour - pidufour@deloitte.ca
* 
*****************************************************************************************************************************************/
global class DTT_Updater  implements Database.Batchable<sObject> {
       
 
        private string m_Obj;
        private string m_Filter = 'Id != null';
        private string m_ResetField = null;
       
        global DTT_Updater(string ObjectName){
                m_Obj = ObjectName;
        }
 
        global DTT_Updater(string ObjectName, string filter){
                m_Obj = ObjectName;
                m_Filter = filter;
        }
       
        global DTT_Updater(string ObjectName, string filter, string resetField){
                m_Obj = ObjectName;
                m_Filter = filter;
                m_ResetField = resetField;
        }
       
        global Database.QueryLocator start(Database.BatchableContext bc) {
                        String query = 'Select Id from ' +m_Obj + ' where ' + m_Filter;
                return Database.getQueryLocator(query);
        }
       
        global void execute(Database.BatchableContext BC, list<Sobject> scope){
 
                if (m_ResetField != null)
                        for (Sobject s : scope)
                                s.put(m_Resetfield, null);
                       
                Database.update(scope,false);
       
        }
       
        global void finish(Database.BatchableContext BC){
               
               
       
        }
 
       
       
}