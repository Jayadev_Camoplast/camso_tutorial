/*********************************************************************************
Class Name      : CS_ReUsableComponentProductSelection
Description     : This is class is the extension controller for product selection reusable component
Created By      : Divya A N
Created Date    : 2014-08-12
Modification Log:
---------------------------------------------------------------------------------- 
Developer                   Date                   Description
----------------------------------------------------------------------------------            
Divya                    12-08-2014            Initial Version
*********************************************************************************/

public with sharing class CS_ReUsableComponentProductSelection {
    
    public list<SObject> reusableVar{get; set;}
    public list<SObject> ShoppingCart{get; set;}
    
    public list<OpportunityLineItem> OpportunityShoppingCart{get
        {
            //return  (list<OpportunityLineItem>) ShoppingCart;
            return  new list<OpportunityLineItem>();
        }
     set;}
    
    
    public SObject reuse{get; set;}
    public Order theOrder{get; set;}
    public Opportunity theOpp{get; set;}
    public String searchString {get;set;}
    public priceBookEntry[] AvailableProducts {get;set;}
    public Pricebook2 theBook {get;set;}  
    public String ChildObject{get; set;}
    public String LookupId {get; set;}
    public String AddedDiscount {get; set;}
    public String Discount {get; set;}
    public String Quantity {get; set;}
    public String TotalPrice {get; set;}
    public String UnitPrice {get; set;}
    public Decimal totalVolume{get; set;}
    public String Weight {get; set;}
    public String Volume {get; set;}
    public String WeightVolRatio {get; set;}
    Public Boolean bGetDataFromExtSystem {get;set;}
    Public String sSelectedLocation {get;set;}
    Public String sSelectedProductLocation {get;set;}
    Public string recId{get; set;}
    
    public String query{get; set;}
    public String queryChild{get; set;}
    public String toSelect {get; set;}
    public String toUnselect {get; set;}
    public string ChosenValue{get;set;}
    public Decimal Total {get;set;}
    public Decimal Totalsales {get; set;}
    public Decimal prodweight{get; set;}
    //public Decimal addeddiscount{get; set;}
    public Boolean freightdisc {get; set;}
    public Boolean weightdisc {get; set;}
    
    
    public Boolean overLimit {get;set;}
    public Boolean multipleCurrencies {get; set;}
    
    private Boolean forcePricebookSelection = false;
    public Boolean OpportunityPage = false;
    public Boolean OrderPage = false;
    
    private SObject[] forDeletion ;
    public String qString{get;set;}


    public CS_ReUsableComponentProductSelection(ApexPages.StandardController controller) {
         String type = controller.getRecord().getSObjectType().getDescribe().getName();

        // Need to know if org has multiple currencies enabled
        multipleCurrencies = UserInfo.isMultiCurrencyOrganization();

        // Get information about the Opportunity being worked on
        Id recId;
        if(controller.getRecord() != NULL) {
            recId = controller.getRecord().Id;
            
        }
        if(recId == NULL) {
            recId = ApexPages.currentPage().getParameters().get('ordId');
            
        }
        if(recId == NULL) {
            recId = ApexPages.currentPage().getParameters().get('addTo');
            
        }
            

        
        if(type == 'Opportunity'){
        OpportunityPage = true;
        forDeletion = new list<OpportunityLineItem> ();
        ShoppingCart = new list<OpportunityLineItem> ();
        }
        
        if(type == 'Order'){
        OrderPage = true;
        forDeletion = new list<OrderItem> ();
        ShoppingCart = new list<OrderItem>();
        }
        String objectName = type;
        Map<String, Reusable_Component__c> reusableComps = Reusable_Component__c.getAll();
        if(reusableComps.containsKey(objectName)){
            ChildObject = reusableComps.get(objectName).Child_Object_Name__c;
            LookupID = reusableComps.get(objectName).Look_Up_ID__c;
            AddedDiscount = reusableComps.get(objectName).Added_Discount__c;
            Discount = reusableComps.get(objectName).Discount__c;
            Quantity = reusableComps.get(objectName).Quantity__c;
            TotalPrice = reusableComps.get(objectName).Total_Price__c;
            UnitPrice = reusableComps.get(objectName).Unit_Price__c;
            Weight = reusableComps.get(objectName).Weight__c;
            Volume = reusableComps.get(objectName).Volume__c;
            WeightVolRatio = reusableComps.get(objectName).Wt_Vol_Ratio__c;
            
        }
        
            query = 'SELECT';
            Map<String, Schema.SObjectField> objectFields = Schema.getGlobalDescribe().get(objectName).getDescribe().fields.getMap();
            // Grab the fields from the describe method and append them to the queryString one by one.
            for(String s : objectFields.keySet()) {
                Schema.DescribeFieldResult F = objectFields.get(s).getDescribe();
              
                   query += ' ' + s + ',';
            }
            query += 'Pricebook2.Name,Ship_To_Account__r.Default_Location__r.Name';
            // Strip off the last comma if it exists.
            if (query.subString(query.Length()-1,query.Length()) == ','){
                query = query.subString(0,query.Length()-1);
            }
            
            query += ' FROM ' + objectName;
            query += ' WHERE Id=\''+recId+'\''; 
            
           list<sObject> reusableVar = database.query(query);
           system.debug(reusableVar + '****');
            for(SObject each:reusableVar){
            reuse= each;
            
            }
            system.debug(reuse+ '###');
            system.debug(reuse.get('Pricebook2Id')+ '&&&');
            system.debug(string.valueof(reuse.get('Pricebook2Id')) + '  ' +'!!!');
            //ID qID =reuse.get('CS_Quote_Price_Book__c'); 
            queryChild = 'SELECT';
            Map<String, Schema.SObjectField> ChildObjectFields = Schema.getGlobalDescribe().get(ChildObject).getDescribe().fields.getMap();
            // Grab the fields from the describe method and append them to the queryString one by one.
            for(String s : ChildObjectFields.keySet()) {
                Schema.DescribeFieldResult F = ChildObjectFields.get(s).getDescribe();
                   queryChild += ' ' + s + ',';
            }
            queryChild += 'PriceBookEntry.UnitPrice, PriceBookEntry.Name,PriceBookEntry.IsActive, PriceBookEntry.Product2Id, PriceBookEntry.Product2.Name,PriceBookEntry.Product2.Location__c, PriceBookEntry.Product2.Family, PriceBookEntry.Product2.Weight__c , PriceBookEntry.Product2.ProductCode, PriceBookEntry.Product2.Global_PID__c, PriceBookEntry.PriceBook2Id ';
            // Strip off the last comma if it exists.
            if (queryChild.subString(queryChild.Length()-1,queryChild.Length()) == ','){
                queryChild = queryChild.subString(0,query.Length()-1);
            }
            queryChild += ' FROM ' + ChildObject;
            queryChild += ' WHERE ' + LookupID + '=\''+recId+'\''; 
      
    
        ShoppingCart = database.query(queryChild);
        //Update Total Price & Weight
        updatePriceAndWeight();
        // Check if Opp has a pricebook associated yet
       
        if(reuse.get('Pricebook2Id') == null){
            system.debug('Hello!');
            Pricebook2[] activepbs = [select Id, Name from Pricebook2 where isActive = true limit 2];
            if(activepbs.size() == 2){
                forcePricebookSelection = true;
                theBook = new Pricebook2();
            }
            else{
                theBook = activepbs[0];
            }
        }
        
        else{
        theBook = new PriceBook2();
        theBook.Id = String.valueOf(reuse.get('Pricebook2Id'));
        }
    }
     
            
    // this is the 'action' method on the page
    public PageReference priceBookCheck(){
    
        // if the user needs to select a pricebook before we proceed we send them to standard pricebook selection screen
        if(forcePricebookSelection){        
            return changePricebook();
        }
        else{
        
            //if there is only one active pricebook we go with it and save the opp
            if(string.valueOf(reuse.get('pricebook2Id')) != theBook.Id){
                try{
                    reuse.put('Pricebook2Id',theBook.Id);
                    update(reuse);
                }
                catch(Exception e){
                    ApexPages.addMessages(e);
                }
            }
            
            return null;
        }
    }
       
    public String getChosenCurrency(){
    
        if(multipleCurrencies)
            return (String)reuse.get('CurrencyIsoCode');
        else
            return '';
    }

    public void updateAvailableList() {
    
        // We dynamically build a query string and exclude items already in the shopping cart
        //Divya:Search Using Customer Item Number
            list<Custom_PID_Link__c> customPIDLink = [SELECT Product__c from Custom_PID_Link__c where Customer_Item_Number__c like : searchString];
            set<ID> forProducts = new set<ID>();
            for(Custom_PID_Link__c cPID:customPIDLink)
               forProducts.add(cPID.Product__c);
        qString = 'select Id, Pricebook2Id, IsActive, Product2.Name,Product2.Location__c, Product2.Family,Product2.Product_Line__c, Product2.IsActive,Product2.ProductCode,Product2.ABC_Classification__c,Product2.Description, UnitPrice,Product2.Weight__c,Product2.Global_PID__c,Product2.On_hand__c,Product2.Reserved__c,Product2.Ordered__c,Product2.Transit__c,Product2.AvailableInventory__c,Product2.Brand__c from PricebookEntry where IsActive=true and Pricebook2Id = \'' + theBook.Id + '\'';
        if(multipleCurrencies)
            qstring += ' and CurrencyIsoCode = \'' + reuse.get('currencyIsoCode') + '\'';
        
        // note that we are looking for the search string entered by the user in the name OR description
        // modify this to search other fields if desired
        if(searchString!=null){
           qString += string.format('and (Product2.Name like \'\'%{0}%\'\' or Product2.Description like \'\'%{0}%\'\' or Product2.Id in : forProducts)', new string[]{searchString});
        }
        
        Set<Id> selectedEntries = new Set<Id>();
        Totalsales=0.0;
        prodweight=0.0;
            for(SObject d:ShoppingCart){
                string var = string.valueOf(d.get('PricebookEntryId'));
                selectedEntries.add(var);
                
            }
            updatePriceAndWeight();
       
        if(selectedEntries.size()>0){
            String tempFilter = ' and Id not in (';
            for(Id i : selectedEntries){
                tempFilter+= '\'' + (String)i + '\',';
            }
            String extraFilter = tempFilter.substring(0,tempFilter.length()-1);
            extraFilter+= ')';
            
            qString+= extraFilter;
        }
        
        qString+= ' order by Product2.Name';
        qString+= ' limit 101';
        
        system.debug('qString:' +qString);        
        AvailableProducts = database.query(qString);
        
        // We only display up to 100 results... if there are more than we let the user know (see vf page)
        if(AvailableProducts.size()==101){
            AvailableProducts.remove(100);
            overLimit = true;
        }
        else{
            overLimit=false;
        }
         
        CS_REST_TestingController.bGetDataFromExtSystem = bGetDataFromExtSystem;
        // Fill the inventory related columns (from the Callout)
        //AvailableProducts = CS_REST_TestingController.UpdateInventoryDetails(AvailableProducts,sSelectedLocation);
    }
    
    public void addToShoppingCart(){
    
        // This function runs when a user hits "select" button next to a product
    
        for(PricebookEntry d : AvailableProducts){
            system.debug(OpportunityPage + '  ' +'++++');
            if(OpportunityPage == true){
                if((String)d.Id==toSelect  && d.Product2.Location__c == sSelectedProductLocation){
                    shoppingCart.add(new opportunityLineItem(OpportunityId=recId, PriceBookEntry=d, PriceBookEntryId=d.Id, UnitPrice=d.UnitPrice));
                    break;
                }
            }
            if(OrderPage == true){
                if((String)d.Id==toSelect  && d.Product2.Location__c == sSelectedProductLocation){
                    shoppingCart.add(new OrderItem(OrderId=recId, PriceBookEntry=d, PriceBookEntryId=d.Id, UnitPrice=d.UnitPrice));
                    break;
                }
            }
        }
        
        // While adding to Shopping Cart no need to fetch the inventory data from external system. 
        bGetDataFromExtSystem = false;
        updateAvailableList();  
    }
    
    public list<Product2> getProductlist 
    
    {get
        {
            
            ID PBId=string.ValueOf(reuse.get('Pricebook2Id'));
         
            return[select name from product2 where Id in (select Product2Id from PricebookEntry where Pricebook2Id=:PBId)limit 999];
        }
    }
    public PageReference removeFromShoppingCart(){
    
        // This function runs when a user hits "remove" on an item in the "Selected Products" section
    
        Integer count = 0;
        if(OrderPage == true){
            for(SObject productItem : ShoppingCart){
                if((String)productItem.get('PriceBookEntryId')==toUnselect){
            
                    if(productItem.get('Id')!=null)
                        forDeletion.add(productItem);
            
                    ShoppingCart.remove(count);
                    break;
                }
                count++;
            }
        }
        updateAvailableList();
        
        return null;
    }
    
    public PageReference onSave(){
    
        // If previously selected products are now removed, we need to delete them
        if(forDeletion.size()>0)
            delete(forDeletion);
         //To Apply Entered Discounts on Save if Apply Discount is not clicked by the user manually
        applyDiscount();
        
      
    
        // Previously selected products may have new quantities and amounts, and we may have new products listed, so we use upsert here
        try{
            if(ShoppingCart.size()>0)
                upsert(ShoppingCart);
            upsert(reusableVar);
          }
        catch(Exception e){
            ApexPages.addMessages(e);
            return null;
        }  
        // After save return the user to the Order
        return new PageReference('/' + ApexPages.currentPage().getParameters().get('Id'));
    }
    
    public PageReference onCancel(){
 
        // If user hits cancel we commit no changes and return them to the Order   
        return new PageReference('/' + ApexPages.currentPage().getParameters().get('Id'));
    }
    
   //Divya-Action for Apply Discount button
     //Divya-Action for Apply Discount button
    public PageReference applyDiscount(){
    
    Totalsales =0.0;
    totalVolume = 0.0;
        for(SObject scart : ShoppingCart) {        
              
            if(decimal.valueOf(string.valueOf(scart.get(Discount))) != null && decimal.valueOf(string.valueOf(scart.get(Discount)))>= 0 && decimal.valueOf(string.valueOf(scart.get(Discount))) <=100)
             scart.put(TotalPrice,(decimal.valueOf(string.valueOf(scart.get(UnitPrice))) - (decimal.valueOf(string.valueOf(scart.get(UnitPrice))))    * Decimal.valueOf(string.valueOf(scart.get(Discount)))/100));
         
            else if(scart.get(Discount) == null && (scart.get(TotalPrice)!= scart.get(UnitPrice))){
                scart.put(TotalPrice, (decimal.valueOf(string.valueOf(scart.get(UnitPrice)))));
                
            }
            else if(decimal.valueOf(string.valueOf(scart.get(Discount))) < 0 || decimal.valueOf(string.valueOf(scart.get(Discount))) >100) {
                ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Discount can not be negative or greater than 100%.'));
                break;
            }              
             
          //  Totalsales = Totalsales + (scart.get(TotalPrice)* scart.get(Quantity)); 
            if(decimal.valueOf(string.valueOf(scart.get(Volume)))!= null && decimal.valueOf(string.valueOf(scart.get(Quantity)))!= null && decimal.valueOf(string.valueOf(scart.get(Weight)))!=null)
            {   {   (string.valueOf(scart.put(WeightVolRatio,(String.valueOf( ((Decimal.valueOf(String.valueOf(scart.get(Weight)))) * (decimal.valueOf(String.valueOf(scart.get(Quantity))))).intValue()) +  '/' +(decimal.valueOf(string.valueOf(scart.get(Volume)))*decimal.valueOf(string.valueOf(scart.get(Quantity)))).intValue()))));
                totalVolume = totalVolume + decimal.valueOf(string.valueOf((scart.get(Volume)))) * decimal.valueOf(string.valueOf(scart.get(Quantity))); 
            }        
        }
        //Maryam - Refreshing weight and volume before calculation added discount
        prodweight =0.0;
        totalVolume =0.0;
        for(SObject scart1 : shoppingCart) { 
                    
            if((scart1.get(Weight)!= null) && (scart1.get(Weight) != '')&& scart1.get(Quantity)!= null)
            {
                prodweight = prodweight + (Decimal.Valueof(string.ValueOf(scart1.get(Weight))) * decimal.valueOf(string.valueOf(scart1.get(Quantity)))); 
            }   
            if(scart1.get(Volume)!= null && scart1.get(Quantity)!= null)
            {
                totalVolume= totalVolume + (Decimal.Valueof(string.ValueOf(scart1.get(Volume))) * Decimal.Valueof(string.ValueOf(scart1.get(Quantity)))); 
            }  
            
        }
        if(Decimal.Valueof(string.ValueOf(reuse.get(AddedDiscount)))< 0 || Decimal.Valueof(string.ValueOf(reuse.get(AddedDiscount))) >100) {
               ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Discount can not be negative or greater than 100%.'));
               
        } 
        //Maryam - Applying added discount, freight and weight deductions
           
        else if(reuse.get(AddedDiscount) != null)        
        {    if(prodweight > Integer.valueOf(Label.CS_Freight_Discount))
             {    reuse.put('Freight_Charges__c', 0); 
                  reuse.put('Freight_Discount_Applied__c', true);             
             }                  
             else
                 reuse.put('Freight_Discount_Applied__c',false); 
             if(prodweight > Integer.valueOf(Label.CS_Weight_Discount))
             {                        
                    if(reuse.get('Weight_Discount_Applied__c')== false || reuse.get('Weight_Discount_Applied__c')==null)
                    {   Totalsales = Totalsales  - (Totalsales * Decimal.Valueof(string.ValueOf(reuse.get(AddedDiscount )))+ 5)/100;      
                        Decimal.Valueof(string.ValueOf(reuse.put(AddedDiscount, Decimal.Valueof(string.ValueOf(reuse.get(AddedDiscount )))+ 5 ))) ;
                        reuse.put('Weight_Discount_Applied__c', true);
                        
                    }
                    else 
                        Totalsales = Totalsales  - (Totalsales * Decimal.Valueof(string.ValueOf(reuse.get(AddedDiscount )))/100);                            
             }            
             else 
             {    if(reuse.get('Weight_Discount_Applied__c')== true)
                  {    
                       if(Decimal.Valueof(string.ValueOf(reuse.get(AddedDiscount))) >=5)
                       {     Totalsales = Totalsales  - (Totalsales * Decimal.Valueof(string.ValueOf(reuse.get(AddedDiscount)))-5/ 100);
                            reuse.put(AddedDiscount,Decimal.Valueof(string.ValueOf(reuse.get(AddedDiscount))) - 5);
                       }                             
                       reuse.put('Weight_Discount_Applied__c',false);
                                               
                  }
                  else
                  {    Totalsales = Totalsales  - (Totalsales * Decimal.Valueof(string.ValueOf(reuse.get(AddedDiscount)))/ 10);
                       
                  }
             } 

        }
        //Maryam added to update added discounts
        reuse.put('Total_Price__c',Totalsales);
      
        }
        return null;
        
    }
    //Maryam Initialize Sales Price
    public PageReference initializeSalesPrice(){
        
         for(SObject scart : shoppingCart) { 
                
                if(scart.get(TotalPrice)== null){
                  // scart.get(TotalPrice)= scart.get(UnitPrice); 
                   
                }            
          }
          return null;
    }
    //Divya:Update Price & Weight
    public PageReference updatePriceAndWeight(){
        
                Totalsales=0.0;
                prodweight=0.0;
                totalVolume = 0.0;
                system.debug(shoppingCart + '%%%%');
        for(SObject scart : shoppingCart) { 
                system.debug(scart + '$$$$' + UnitPrice + '%%%%'+ Quantity + '^^^');
                //system.debug(scart.get(UnitPrice)+ '%%%' +scart.get(Quantity));
                
            if((scart.get(UnitPrice)!= null && Decimal.valueOf(string.valueOf(scart.get(UnitPrice)))!= 0 && (scart.get(Quantity))!= null)){
            
               Totalsales = Totalsales + (Decimal.valueOf(string.valueOf(scart.get(UnitPrice)))* Decimal.valueOf(string.valueOf(scart.get(Quantity)))); 
               
            }
           
            if((scart.get(Weight)!= null) && (scart.get(Weight) != '')&& scart.get(Quantity)!= null)
            {
                prodweight = prodweight + (Decimal.Valueof(string.valueOf(scart.get(Weight))) * Decimal.Valueof(string.valueOf(scart.get(Quantity)))); 
            }   
            //Maryam added volume calculation
            if(scart.get(Volume)!= null && scart.get(Quantity)!= null&& scart.get(Weight)!=null)
            {    (string.valueOf(scart.put(WeightVolRatio,(String.valueOf( ((Decimal.valueOf(String.valueOf(scart.get(Weight)))) * (decimal.valueOf(String.valueOf(scart.get(Quantity))))).intValue()) +  '/' +(decimal.valueOf(string.valueOf(scart.get(Volume)))*decimal.valueOf(string.valueOf(scart.get(Quantity)))).intValue()))));
                totalVolume = totalVolume + decimal.valueOf(string.valueOf((scart.get(Volume)))) * decimal.valueOf(string.valueOf(scart.get(Quantity)));
            }
           
        }
  
 
        return null;
    } 
    
      public List<SelectOption> getCountriesOptions(){
            list<Location__c> lstLocation = [SELECT Name FROM Location__c WHERE Active__c= True ORDER BY Name];
            List<SelectOption> Options = new List<SelectOption>();
            //LMK 8/11/2014: Added this to display the default value.
            /* String def = String.valueOf(reuse.get('Ship_To_Account__r.Default_Location__r.Name'));
             if(def!=null)
             {
            Options.add(new SelectOption('Select',def));
            }*/
           
                for(Location__c oL: lstLocation)
                {
                    
                    Options.add(new SelectOption(oL.Name, oL.Name));
                }
        return Options;
    }  
    public String getsSelectedLocation () {
        return sSelectedLocation;
    }        
    
    //[Divya :30-Jul]To store all the values of the Product Family that qualify as "Material"
     public String sValue{ get { return 'Construction Pneumatic;Industrial Pneumatic'; } set; }
    
    public PageReference changePricebook(){
        PageReference ref;
        // This simply returns a PageReference to the standard Pricebook selection screen
        // Note that is uses retURL parameter to make sure the user is sent back after they choose
        
        if(OrderPage== true){
        ref = new PageReference('/_ui/busop/orderitem/ChooseOrderPricebook/e?');
        ref.getParameters().put('id',recId);
        ref.getParameters().put('retURL','/apex/CS_OrderProductEntry?id=' + recId);
        
        }
        
        if(OpportunityPage == true){
        ref = new PageReference('/oppitm/choosepricebook.jsp');
        ref.getParameters().put('id',recId);
        ref.getParameters().put('retURL','/apex/opportunityProductEntry?id=' + recId);
       
        }
       return ref;  
    }
    
     public PageReference details(){
    
        // This function runs when a user hits "select" button next to a product
    
        
        return new PageReference('/'+AvailableProducts.get(0).id);  
    }
}