/*
*   @author Frank Neezen
*   @date 13/11/2012
*   @description Test trigger on Risk Issues object

    Trigger Name:  test_RiskIssueTrigger 
    Version     : 1.0  
    Created Date: 13/11/2012
    Function    :  
    Modification Log :
    -----------------------------------------------------------------------------
    Developer                   Date                    Description
    ----------------------------------------------------------------------------                 
    Frank Neezen             13/11/2012              Original Version
*/
@isTest (SeeAllData=true) 
public class Test_Trigger_Risk_Issues {

   static testMethod void test_RiskIssueTrigger() {
        Risks_Issues__c oRisk = new Risks_Issues__c();  //create new risk record
        oRisk.Date_Raised__c = Date.Today();            //set raised
        oRisk.Title__c = 'Test summary';                //set title
        
        Database.insert(oRisk);                         //Insert into DB
        
        List<Risks_Issues__c> lRisks = [ Select Identified_By__c From Risks_Issues__c Where Id =: oRisk.Id ];   //fetch identify by
        
        //If we have records 
        if(lRisks.size()>0)
            System.assert(lRisks[0].Identified_By__c == UserInfo.getUserId());  //check if the identified by is set to current user 
        
    }
}