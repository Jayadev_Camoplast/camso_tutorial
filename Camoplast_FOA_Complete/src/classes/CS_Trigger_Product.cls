/*********************************************************************************
Class Name      : CS_Trigger_Product
Description     : This class will be invoked by the triggers on Products 
Created By      : Divya A N 
Created Date    : 15-Sep-14
Modification Log:
---------------------------------------------------------------------------------- 
Developer                   Date                   Description
----------------------------------------------------------------------------------            
Divya A N                  15-Sep-14              Initial Version
*********************************************************************************/

public class CS_Trigger_Product{
 
     /*********************************************************************************
     Method Name    : createPriceBookEntry
     Description    : To create a PriceBookEntry record when Product is created
     Return Type    : void
     Parameter      : 1. lstProduct: List of new Product
     *********************************************************************************/
    

    public static void createPriceBookEntry(List<Product2> lstProduct)
    {
        List<PricebookEntry> lstPbe=new list<PricebookEntry>();
                PricebookEntry PB;
        List<Pricebook2> lstPriceBook = [SELECT id FROM Pricebook2 where IsStandard= True LIMIT 1];
        if(lstPriceBook != null && lstPriceBook.size() > 0){
            Id PriceBookId = lstPricebook[0].Id;
            //List of all the Currencies that are active
            List<CurrencyType> lstCurrency = [SELECT ISOCode, ConversionRate FROM CurrencyType WHERE IsActive=TRUE];
            
            for(integer i = 0; i< lstCurrency.size(); i++){
                for(Product2 oProduct : lstProduct){
                
                    PB=new PricebookEntry();
                    PB.IsActive=true;
                    PB.Pricebook2Id= PriceBookId;
                    PB.IsActive= True;
                    PB.Product2Id = oProduct.Id;
                    PB.CurrencyISOCode = lstCurrency[i].ISOCode;
                    decimal convertedCurrency = lstCurrency[i].ConversionRate;
                    // The Unit Price will be 1 Unit of the Currency from the Conversion factor
                    PB.UnitPrice= 1 * convertedCurrency;
                    
                    lstPbe.add(PB);
                }
                
            }
          }
            insert lstPbe;
        
    }
}