/*********************************************************************************
Class Name      : CS_Test_Opportunity_Line_Item_Update
Description     : This class is used for Test Classes for Functionalities on Opportunity Line Item (Opportunity Product)object
Created By      : Laxmy M Krishnan
Created Date    : 16-Jul-14
Modification Log:
---------------------------------------------------------------------------------- 
Developer                   Date                   Description
----------------------------------------------------------------------------------            
Laxmy M Krishnan              16-Jul-14               Initial Version
*********************************************************************************/
@isTest(SeeAllData=true)
public class CS_Test_Opportunity_Line_Item_Update{
 /*********************************************************************************
 Method Name    : test_updateApprovalStatusOppoLi
 Description    : Test method to update Approval Status based on Sales Price.
 Return Type    : void
 Parameter      : nil                 
 *********************************************************************************/
    
       static testmethod void test_updateApprovalStatusOppoLi(){
       
       
        list <Opportunity> Oppo = [Select Id,CurrencyIsoCode,Pricebook2Id from Opportunity where AccountId != null limit 1];
        ID OppId= Oppo.get(0).Id;
        ID PbId=Oppo.get(0).Pricebook2Id;
        String Cur=Oppo.get(0).CurrencyIsoCode;
        
        
        list<PricebookEntry> Pbe = [Select Id,CurrencyIsoCode,  Product2Id from PricebookEntry where Pricebook2Id=:PbId AND CurrencyIsoCode=:Cur limit 1];
        ID PbeId= Pbe.get(0).Id;

        OpportunityLineItem Oli= new OpportunityLineItem();
        Oli.OpportunityId=OppId;
        Oli.PriceBookEntryId=PbeId;
        Oli.Quantity=10;
       Oli.UnitPrice=100;
       //Oli.UnitPrice=Oli.ListPrice;
        //Oli.TotalPrice= Oli.Quantity*Oli.UnitPrice;       
        insert Oli;
        
test.starttest();               
system.Assert(Oli.Id != null); 
//System.Assert(Oli.Approval_Status__c=='Required');

      
test.stoptest();    

}
}