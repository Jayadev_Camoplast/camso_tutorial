/*************************************************************************************
Class Name : CS_Test_Extension_CreateNewOrder
Description : Create a new order from Quote (Used for the button click on Quote)
Created By  : Divya A N
Created Date: 31-July-14
Modification Log:
---------------------------------------------------------------------------------- 
Developer             Date             Description
---------------------------------------------------------------------------------- 
Divya A N         31-Jul-2014        Initial Version
************************************************************************************/
@istest (seeAllData= true)
private class CS_Test_Extension_CreateNewOrder{

    static testMethod void CreateNewOrder(){
         list<Pricebook2> oPb2 = [Select Id from PriceBook2 where isStandard = true];
           Pricebook2 oPb=new Pricebook2();
                 oPb.Name= 'Test';
                 oPb.Construction_Discount_Class__c= 'C';
                 oPb.MH_Discount_Class__c= 'B';
                 oPb.IsActive= true;
             insert oPb;
           
           datetime myDateTime = datetime.now();
             test.starttest();     
             Account oAcc= new Account();
                 oAcc.Name = 'Test Account';
                 oAcc.CurrencyIsoCode = 'EUR';            
                 oAcc.Con_Discount_Class__c = 'C';
                 oAcc.MH_Discount_Class__c =  'B';                 
             insert oAcc;
             Product2 oPr= new Product2();
                    oPr.Name = 'Test Product';
                    oPr.CurrencyIsoCode = 'USD';
                    //oPr.DefaultPrice = 1200;
                    oPr.Family = 'Service';
                    oPr.IsActive = true;
                    oPr.ProductCode = 'Test123';
                    
            insert oPr;
             
              Terms_and_Conditions__C terms = new Terms_and_Conditions__c();
             terms.Terms_and_Conditions__c = 'New Term';
             terms.AccountId__c = oAcc.ID;
             insert terms;
            
            PricebookEntry oPbe0 = new PricebookEntry();
                oPbe0.Pricebook2id = oPb2[0].Id;
                oPbe0.Product2id = oPr.Id;
                //oPbe0.UseStandardPrice = true;
                //StandardPrice = 1200;
                oPbe0.UnitPrice = 1100;
            insert oPbe0;

            
            PricebookEntry oPbe = new PricebookEntry();
                oPbe.Pricebook2id = oPb.Id;
                oPbe.Product2id = oPr.Id;
                oPbe.UseStandardPrice = false;
                //StandardPrice = 1200;
                oPbe.UnitPrice = 1000;
                oPbe.IsActive = true;
            insert oPbe; 
            
             Opportunity opp= new Opportunity();
                 opp.Name = 'Test Opty';
                 opp.CloseDate = myDateTime.date() ;            
                 opp.StageName = 'Qualification';
                 opp.AccountId =  oAcc.ID;                 
             insert opp; 
                   
             Quote quote = new Quote();
                 quote.Name = 'Test Quote';
                 quote.OpportunityId = opp.Id;
                 quote.Pricebook2Id= oPb.Id;
             insert quote;
             
             QuoteLineItem quoteLineItem = new QuoteLineItem(QuoteId=quote.Id,PriceBookEntryId=oPbe.Id,Quantity= 5.0,UnitPrice= 1000,Product2Id= oPr.Id);
              insert quoteLineItem;
            
            //Load the VF Page  
            PageReference pageRef = Page.CS_CreateNewOrder;
            pageRef.getParameters().put('Id',quote.Id);
            Test.setCurrentPageReference(pageRef);
        
            // load the extension
            CS_Extension_CreateNewOrder createNewOrder = new CS_Extension_CreateNewOrder(new ApexPages.StandardController(quote));
            
            //Create Order Button
            createNewOrder.createOrder();
            
         
            
    }
 }