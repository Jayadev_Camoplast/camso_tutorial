public class treenodes {

/* Wrapper class to contain the nodes and their children */
public class cNodes
{

 public List<Substitute_Product__c> parent {get; set;}
 Public Product2 gparent {get;set;}

 public cNodes(Product2  gp, List<Substitute_Product__c> p)
 {
     parent = p;
     gparent = gp;
 }
}
/* end of Wrapper class */ 

Public List<cNodes> hierarchy;

Public List<cNodes> getmainnodes()
{
    hierarchy = new List<cNodes>();
    List<Product2> tempparent = [Select Id,Name,Product_Line__c, ProductCode, Global_PID__c, ABC_Classification__c,Location__c,On_hand__c,Reserved__c,Ordered__c,Transit__c,AvailableInventory__c from Product2 limit 10];
    for (Integer i =0; i< tempparent.size() ; i++)
    {
        List<Substitute_Product__c> tempchildren = [Select Id,Name,CurrencyIsoCode,Additional_Information__c,Priority__c from Substitute_Product__c where Parent_Product__c = :tempparent[i].Id limit 10];
        hierarchy.add(new cNodes(tempparent[i],tempchildren));
     }   
    return hierarchy;
}   
}