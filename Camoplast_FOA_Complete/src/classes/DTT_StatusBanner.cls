public class DTT_StatusBanner {
	
    public Account acc {get;set;}
    
    public DTT_StatusBanner(ApexPages.StandardController controller) {

        if (controller != null)
        {
            if (controller.getRecord() != null)
            {
                List<Account> accs = [SELECT Id, Sync_Status_Icon__c, Last_Sync_Datetime_with_ERP__c
                            FROM Account
                            LIMIT 1];
                
                 for (Account a : accs)
                 {
					acc = a;
                 }
            }
        }
        
		/* TODO: Initialize all variables to avoid null exceptions or page defaults */
        /* TODO: check for Id in Query_String */
        /* TODO: check for prefix and object type from Id */
        /* TODO: query record and retrieve status fields */        
    }

    
}