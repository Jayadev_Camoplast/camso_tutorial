@isTest(seeAllData = false)
Public class Test_DocumentController{

static testMethod void DocumentController(){

Project__c pro = new Project__c();
pro.Name = 'Test Project';
insert pro;

Sprint__c  sp = new Sprint__c();
sp.Name = 'Test Sprint 1';
insert sp;

User_Story__c r = new User_Story__c();
r.Allocated_Sprint__c = sp.Id;
r.Functional_Area__c  = 'SFA';
r.Sub_Process__c = 'Marketing';
r.User__c = UserInfo.getUserId();
r.Supporting_BA__c = UserInfo.getUserId();
r.Development_Stage__c = '9. Done';

insert r;


UserStory2Design__c  usd = new UserStory2Design__c();
usd.User_Story__c = r.Id;
insert usd;

DocumentController TDC = new DocumentController();

TDC.getUserStories(); 
TDC.initUS= r ;
TDC.createQuerySOQL();
TDC.toggleSort();
DocumentController.cUS  cus = new DocumentController.cUS(r);
cus.selected = true;
TDC.userStoriesList.add(cus);
TDC.processSelected();
TDC.selectedUS.add(r);
TDC.createDocument();
TDC.CreateFunctionalDocument();
TDC.generateQAcards();


delete r;
DocumentController TDC1 = new DocumentController();
TDC1.getUserStories();


} 

}