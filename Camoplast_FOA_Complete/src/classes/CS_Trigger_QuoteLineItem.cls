/*********************************************************************************
Class Name : CS_Trigger_QuoteLineItem
Description : This class will be invoked by the triggers on Quote
Created By : Maryam Abbas
Created Date : 8-27-2014
Modification Log:
---------------------------------------------------------------------------------- 
Developer Date Description
---------------------------------------------------------------------------------- 
Maryam Abbas  8-27-2014   Initial Version
*********************************************************************************/

public class CS_Trigger_QuoteLineItem
{
    public static void copyOptyLineItemInfo(List<QuoteLineItem> lstQuoteItem)
    {  
         String mapKey;
         Set<Id> CS_setQuotePBEIds= new Set<Id>();
         Set<Id> CS_setOptyIds= new Set<Id>();
         Set<Id> CS_setQuoteIds= new Set<Id>();
       
         for(QuoteLineItem oQuoteItem: lstQuoteItem){
             CS_setOptyIds.add(oQuoteItem.OpportunityId__c);
             CS_setQuoteIds.add(oQuoteItem.QuoteId);
             CS_setQuotePBEIds.add(oQuoteItem.PriceBookEntryId);
         }
         
         List<Quote> lstQuote = [Select Id, OpportunityId from Quote where Id in :CS_setQuoteIds];
         
         for(Quote oQuote: lstQuote){
             CS_setOptyIds.add(oQuote.OpportunityId);            
         }   
         
         List<QuoteLineItem> lstQuoteItems = [Select Id, Quote.OpportunityId, Wheel_Position__c, Location__c, PriceBookEntryId from QuoteLineItem where QuoteId in :CS_setQuoteIds];
         Map<String, OpportunityLineItem> mapOptyProducts = new Map<String, OpportunityLineItem>();
         List<OpportunityLineItem> lstOptyItem = [Select Id, PriceBookEntryId, OpportunityId, Location__c, Wheel_Position__c, Tyre_Condition__c,Torque_Setting__c,Wex__c,Fitted_On_Site__c,ListPrice, UnitPrice, Sales_Price__c,Discount,Quantity, Quantity__c from OpportunityLineItem where PriceBookEntryId in :CS_setQuotePBEIds and OpportunityId in :CS_setOptyIds];// and 
      
        //Location__c
         for(OpportunityLineItem oOptyItem : lstOptyItem )
         {
            mapKey = String.valueOf(oOptyItem.PriceBookEntryId) + String.valueOf(oOptyItem.OpportunityId);
            mapOptyProducts.put(mapKey,oOptyItem);              
         }  
         if(mapOptyProducts!= null && mapOptyProducts.IsEmpty()== false){
             for (QuoteLineItem CS_oQuoteItem : lstQuoteItems){
                mapKey =String.valueOf(CS_oQuoteItem.PriceBookEntryId) + String.valueOf(CS_oQuoteItem.Quote.OpportunityId);
                    CS_oQuoteItem.Wheel_Position__c = mapOptyProducts.get(mapKey).Wheel_Position__c ;
                    CS_oQuoteItem.Tyre_Condition__c = mapOptyProducts.get(mapKey).Tyre_Condition__c;
                    CS_oQuoteItem.Torque_Setting__c = mapOptyProducts.get(mapKey).Torque_Setting__c;
                    CS_oQuoteItem.Wex__c = mapOptyProducts.get(mapKey).Wex__c;
                    CS_oQuoteItem.Fitted_On_Site__c = mapOptyProducts.get(mapKey).Fitted_On_Site__c;
                    CS_oQuoteItem.UnitPrice = mapOptyProducts.get(mapKey).Sales_Price__c;
                    CS_oQuoteItem.Discount= mapOptyProducts.get(mapKey).Discount;
                    CS_oQuoteItem.Quantity= mapOptyProducts.get(mapKey).Quantity;
                    CS_oQuoteItem.Location__c= mapOptyProducts.get(mapKey).Location__c;
                    //changed the Location to lookup
            //CS_oQuoteItem.Quantity__c= mapOptyProducts.get(mapKey).Quantity__c;
               
             }
         }
         update lstQuoteItems;     
   }
}