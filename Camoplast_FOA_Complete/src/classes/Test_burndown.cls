@isTest 
public class Test_burndown {

public static testMethod void test_burndown() {

Project__c testProject = new Project__c();
    testProject.Name = 'TestProject';
    insert TestProject;
    
User_Story__c testRequirement = new User_Story__c();
    testRequirement.Project__c = TestProject.id;
    testRequirement.Acceptance_Criteria__c = 'OldAcceptanceCriteria';
    testRequirement.Compliance_Criteria__c = 'OldCC';
    insert testRequirement;


 Sprint__c currentsprint = New Sprint__c(
  Start_Date__c = System.today(), 
  End_Date__c = System.today().addDays(10),
  Number_Working_Days_in_Sprint__c = 5,
  Project__c = TestProject.Id);
    insert currentSprint;
  
  String sprintId = String.ValueOf(currentsprint.ID);
  
 automateBurnDown.generateSprintBurndown(sprintID);
  
  
  }
  
  
  
  }