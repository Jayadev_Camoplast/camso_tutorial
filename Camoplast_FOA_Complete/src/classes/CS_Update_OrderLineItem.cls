/*********************************************************************************
Class Name : CS_Update_OrderLineItem
Description : This class will be invoked by the triggers on Order
Created By : Laxmy M Krishnan
Created Date : 8-27-2014
Modification Log:
---------------------------------------------------------------------------------- 
Developer Date Description
---------------------------------------------------------------------------------- 
Laxmy M Krishnan  8-27-2014   Initial Version
Reju              9- 2-2014  Added quantity fields
Maryam            9-15-2014  updateOrderItemNumber 
*********************************************************************************/

public class CS_Update_OrderLineItem
{
    
    public static void updateOrderItemNumber(List<OrderItem> lstOrderItem)
    { 
        Set<Id> CS_setOrderIds= new Set<Id>();       
        //Getting the key Ids for every Order Item record.
        for(OrderItem oOrderItem: lstOrderItem){
            CS_setOrderIds.add(oOrderItem.OrderId);          
        }
        List<Order> lstOrder = [Select Id, Order_Item_Max_Number__c from Order where Id in :CS_setOrderIds];
        Map<Id, Decimal> mapItemNumber = new Map<Id, Decimal>();    
        for(Order oOrder: lstOrder){
           // for(OrderItem oOrderItem: lstOrderItem){
                mapItemNumber.put(oOrder.Id,oOrder.Order_Item_Max_Number__c);
           // }
        }
        for(OrderItem oOrderItem: lstOrderItem)
        {
        //JR/Commented since this is throwing an exception when OrderItem.Id is null
            //oOrderItem.Item__c = 10;
            if(mapItemNumber!=null && mapItemNumber.IsEmpty()==false){
                oOrderItem.Item__c=mapItemNumber.get(oOrderItem.OrderId)!=null?(mapItemNumber.get(oOrderItem.OrderId) + 10):10;
                mapItemNumber.put(oOrderItem.OrderId, oOrderItem.Item__c);
            }
        }       
        
    }

    public static void copyQuoteLineItemInfo(List<OrderItem> lstOrderItem)
    { 
    
    //Declaring all the key Ids needed to map between QuoteLineItem and Order Item records.
         Set<Id> CS_setOrderPBEIds= new Set<Id>();
         Set<Id> CS_setQuoteIds= new Set<Id>();
         Set<Id> CS_setOrderIds= new Set<Id>();
       
    //Getting the key Ids for every Order Item record.
        for(OrderItem oOrderItem: lstOrderItem){

            CS_setOrderIds.add(oOrderItem.OrderId);
            CS_setQuoteIds.add(oOrderItem.CS_Quote_ID__c);
            CS_setOrderPBEIds.add(oOrderItem.PriceBookEntryId);
            
        }

   //From OrderItem to Order, bringing values from Parent.
        List<Order> lstOrder = [Select Id, QuoteId from Order where Id in :CS_setOrderIds];

      //Get Quote Id from every Order.
         for(Order  oOrder: lstOrder){
           CS_setQuoteIds.add(oOrder.QuoteId);
            
        }   
  
list<OrderItem>lstOrdItem = [Select Id, Order.QuoteId, Quantity__c, Wheel_Position__c, PriceBookEntryId from OrderItem where OrderId in :CS_setOrderIds];

//Create a map for QuotelineItem between the object and a string. the string will carry unique Id to retrieve data.
        
Map<String, QuoteLineItem> mapQuoteProducts = new Map<String, QuoteLineItem>();

// Get list of QuoteLineItem which has the same PB entry Id and Quote Id as that of Order Line Item.//
List<QuoteLineItem> lstQuoteItem = [Select Id, PriceBookEntryId,QuoteId, Wheel_Position__c, Quantity, Location__c, Tyre_Condition__c, Torque_Setting__c, Wex__c, Fitted_On_Site__c, Discount from QuoteLineItem where PriceBookEntryId in :CS_setOrderPBEIds and QuoteId in :CS_setQuoteIds];

//For every Quote Line Item get the concatenation of QuoteId and PBEId which gives uniqueness.

for(QuoteLineItem oQuoteItem : lstQuoteItem )
        {
           String s = String.valueOf(oQuoteItem.PriceBookEntryId) + String.valueOf(oQuoteItem.QuoteId);
                      
           mapQuoteProducts.put(s, oQuoteItem);              
                               
           
        }  
        String s1;
list<OrderItem>lstOrdItem2 ;

        for (OrderItem CS_oOrderItem : lstOrdItem){
                        s1 =String.valueOf(CS_oOrderItem.PriceBookEntryId) + String.valueOf(CS_oOrderItem.Order.QuoteId);

                       System.Debug('map string for quote' + s1);
           
                       CS_oOrderItem.Quantity = mapQuoteProducts.get(s1).Quantity ;
                       CS_oOrderItem.Wheel_Position__c = mapQuoteProducts.get(s1).Wheel_Position__c ;
                       CS_oOrderItem.Tyre_Condition__c = mapQuoteProducts.get(s1).Tyre_Condition__c ;
                       CS_oOrderItem.Torque_Setting__c = mapQuoteProducts.get(s1).Torque_Setting__c ;
                       CS_oOrderItem.Wex__c = mapQuoteProducts.get(s1).Wex__c ;
                       CS_oOrderItem.Fitted_On_Site__c = mapQuoteProducts.get(s1).Fitted_On_Site__c ;
                       CS_oOrderItem.Discount__c = mapQuoteProducts.get(s1).Discount ;
                       CS_oOrderItem.Quantity = mapQuoteProducts.get(s1).Quantity;
                       CS_oOrderItem.Quantity__c = mapQuoteProducts.get(s1).Quantity;
                       CS_oOrderItem.Location__c = mapQuoteProducts.get(s1).Location__c;
                       //system.debug('@@' +CS_oOrderItem.Location__c + '@@' + mapQuoteProducts.get(s1).Location__c);
                       lstOrdItem2.add(CS_oOrderItem);
            }
update lstOrdItem2;
                
      
}
}