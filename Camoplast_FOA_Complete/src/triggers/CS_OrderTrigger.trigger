/*************************************************************************************
Trigger Name : CS_OrderTrigger
Description : Trigger for 'Order' object. 
Created By : Maryam Abbas
Created Date : 15-Jul-14
Modification Log:
---------------------------------------------------------------------------------- 
Developer Date Description
---------------------------------------------------------------------------------- 
Maryam 15-Jul-14 Initial Version
************************************************************************************/
trigger CS_OrderTrigger on Order(before insert,before update, after insert) {

    /*if(Trigger.isBefore && (Trigger.isInsert ||Trigger.isUpdate)){
        CS_Trigger_Order.updateOrderPriceBook(trigger.new);
    }
    
    if(Trigger.isAfter && Trigger.isInsert){
        CS_Trigger_Order.updateTermsandConditions(trigger.new);
    }*/
}