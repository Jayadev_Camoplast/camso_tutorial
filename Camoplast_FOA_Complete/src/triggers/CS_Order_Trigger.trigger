/*********************************************************************************
Trigger Name    : CS_Order_Trigger
Description     : Trigger for all Order object functionalities
Created By      : Jayadev Rath
Created Date    : 14-Jul-2014
Modification Log:
---------------------------------------------------------------------------------- 
Developer                   Date                   Description
----------------------------------------------------------------------------------            
Jayadev Rath              14-Jul-2014              Initial Version
Divya                     28-Jul-2014              defaultValues
Pierre                    2014-09-01               Location should be updated only on insertion
*********************************************************************************/
trigger CS_Order_Trigger on Order (before Insert, before Update,after Insert, after Update) {
    
    if(trigger.isBefore && trigger.isInsert){
        // Update the location field on Order from Account
        CS_Trigger_Order.updateOrderFields(trigger.New);
       //CS_Trigger_Order.updateOrderBilltoContact(trigger.New);
       // CS_Trigger_Order.updateOrderShiptoAcc(trigger.New);
        CS_Trigger_Order.updateBillToShipToFields(trigger.New);
        CS_Trigger_Order.updateOrderFieldsAcc(trigger.New);
        CS_Trigger_Order.createOrderfromQuote(trigger.New);
        CS_Trigger_Order.updateOrderPriceBook(trigger.new);
        
        
    }
    if(trigger.isBefore && trigger.isUpdate){
        // Update the location field on Order from Account
        
        //Maryam - added this again as we are updating location based on updates to ship to /bill to accounts. We are doing a check prior to updation. User can still overwrite location. 
        CS_Trigger_Order.updateOrderFields(trigger.New);
        CS_Trigger_Order.updateBillToShipToFields(trigger.New);
        // Pierre Dufour - Need to allow override on update.
        //CS_Trigger_Order.updateOrderFields(trigger.New);

       // CS_Trigger_Order.updateOrderBilltoContact(trigger.New);
       // CS_Trigger_Order.updateOrderShiptoAcc(trigger.New);
        //CS_Trigger_Order.updateOrderFieldsAcc(trigger.New); not Required for update as per story comments
        //CS_Trigger_Order.updateOrderPriceBook(trigger.new);
        //CS_Trigger_Order.createOrderfromQuote(trigger.New);
        
       
    }
    if(trigger.isAfter && trigger.isInsert){
        // Send Order Updates to ERP system
        CS_Trigger_Order.SyncJSONDetails(trigger.New);
        CS_Trigger_Order.updateTermsandConditions(trigger.new);
        
        
    }
    if(trigger.isAfter && trigger.isUpdate){
        // Send Order Updates to ERP system
        CS_Trigger_Order.SyncJSONDetails(trigger.New);
    }
}