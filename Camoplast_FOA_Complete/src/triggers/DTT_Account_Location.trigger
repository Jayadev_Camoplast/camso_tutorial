trigger DTT_Account_Location on Account (after insert, after update) {

	if (system.isBatch())
		return;

	if (system.isFuture())
		return;

	if (system.isScheduled())
		return;


	if (trigger.size == 1)
	{
		for (Account a : trigger.new)
		{
			if (a.Last_Geocoding__c == null || a.Last_Geocoding__c < Date.today())
			{
				if (Trigger.isInsert)
					if (a.BillingStreet != null || a.ShippingStreet != null) 
						DTT_LocationCallouts.UpdateAccountFuture(a.Id);

				if (Trigger.isUpdate)
				{
					if (a.BillingStreet != trigger.oldMap.get(a.Id).BillingStreet  || a.ShippingStreet != trigger.oldMap.get(a.Id).ShippingStreet)  
						DTT_LocationCallouts.UpdateAccountFuture(a.Id);
					else if ((a.BillingStreet != null || a.ShippingStreet != null) && a.Last_Geocoding__c == null)
						DTT_LocationCallouts.UpdateAccountFuture(a.Id);
				}
			}
		}
	}
}