<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Location Active</fullName>
        <description>Set Location.Active to true.</description>
        <field>Active__c</field>
        <literalValue>1</literalValue>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Location Active</fullName>
        <actions>
            <name>Location Active</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Location__c.Epicor_Id__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Location__c.Void__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <description>Activate a location coming from Epicor if the field Void is set to false.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
