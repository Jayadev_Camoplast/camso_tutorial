<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>CS_Opportunity Line Approval Update</fullName>
        <field>Approval_Status__c</field>
        <literalValue>Required</literalValue>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Opportunity Approval Update</fullName>
        <field>Approval_Status__c</field>
        <literalValue>Required</literalValue>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>OpportunityId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Opportunity Approval Update_Default</fullName>
        <field>Approval_Status__c</field>
        <literalValue>Not Required</literalValue>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>OpportunityId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Opportunity Line Item Discount Update</fullName>
        <field>Discount</field>
        <formula>0.05</formula>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>test</fullName>
        <field>Notes__c</field>
        <formula>TEXT(PricebookEntry.Product2.Family)</formula>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>CS_Opportunity Approval Update</fullName>
        <actions>
            <name>Opportunity Approval Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>OpportunityLineItem.Approval_Status__c</field>
            <operation>equals</operation>
            <value>Required</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CS_Opportunity Approval Update_Default</fullName>
        <actions>
            <name>Opportunity Approval Update_Default</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>OpportunityLineItem.Approval_Status__c</field>
            <operation>equals</operation>
            <value>Not Required</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CS_Opportunity_Line_Discount</fullName>
        <actions>
            <name>Opportunity Line Item Discount Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>Calculating the Discounts on the Opportunity Line Item base on Total Weight.</description>
        <formula>true</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>UpdateProductFamily</fullName>
        <active>false</active>
        <formula>true</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
