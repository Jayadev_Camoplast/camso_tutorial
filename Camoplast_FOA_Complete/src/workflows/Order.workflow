<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>CS Account Percent Weight Discount</fullName>
        <field>Account_Percent_Wt_Discount__c</field>
        <formula>IF(ISNULL(Ship_To_Account_RelatedAccount__r.Ship_To__r.Discount_on_Weight__c) || ISBLANK(Ship_To_Account_RelatedAccount__r.Ship_To__r.Discount_on_Weight__c)||ISNULL(Ship_To_Account_RelatedAccount__r.Ship_To__r.Minimum_Weight_for_Discount__c) || ISBLANK(Ship_To_Account_RelatedAccount__r.Ship_To__r.Minimum_Weight_for_Discount__c)||ISNULL(text(Ship_To_Account_RelatedAccount__r.Ship_To__r.Minimum_Weight_for_Discount_Unit__c)) || ISBLANK(text(Ship_To_Account_RelatedAccount__r.Ship_To__r.Minimum_Weight_for_Discount_Unit__c)), IF(ISNULL(Account.Discount_on_Weight__c)||ISBLANK(Account.Discount_on_Weight__c)||ISNULL(Account.Minimum_Weight_for_Discount__c)||ISBLANK(Account.Minimum_Weight_for_Discount__c)||ISNULL(text(Account.Minimum_Weight_for_Discount_Unit__c))||ISBLANK(text(Account.Minimum_Weight_for_Discount_Unit__c)),value($Label.CS_Percent_Weight_Discount),Account.Discount_on_Weight__c),Ship_To_Account_RelatedAccount__r.Ship_To__r.Discount_on_Weight__c)</formula>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CS Account Percent Wt Discount</fullName>
        <field>Account_Wt_for_Discount__c</field>
        <formula>IF(ISNULL(Ship_To_Account_RelatedAccount__r.Ship_To__r.Discount_on_Weight__c) || ISBLANK(Ship_To_Account_RelatedAccount__r.Ship_To__r.Discount_on_Weight__c)||ISNULL(Ship_To_Account_RelatedAccount__r.Ship_To__r.Minimum_Weight_for_Discount__c) || ISBLANK(Ship_To_Account_RelatedAccount__r.Ship_To__r.Minimum_Weight_for_Discount__c)||ISNULL(text(Ship_To_Account_RelatedAccount__r.Ship_To__r.Minimum_Weight_for_Discount_Unit__c)) || ISBLANK(text(Ship_To_Account_RelatedAccount__r.Ship_To__r.Minimum_Weight_for_Discount_Unit__c)), IF(ISNULL(Account.Discount_on_Weight__c)||ISBLANK(Account.Discount_on_Weight__c)||ISNULL(Account.Minimum_Weight_for_Discount__c)||ISBLANK(Account.Minimum_Weight_for_Discount__c)||ISNULL(text(Account.Minimum_Weight_for_Discount_Unit__c))||ISBLANK(text(Account.Minimum_Weight_for_Discount_Unit__c)),value($Label.CS_Percent_Weight_Discount),Account.Discount_on_Weight__c),Ship_To_Account_RelatedAccount__r.Ship_To__r.Discount_on_Weight__c)</formula>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CS Account Weight for Discount</fullName>
        <field>Account_Wt_for_Discount__c</field>
        <formula>IF(ISNULL(Ship_To_Account_RelatedAccount__r.Ship_To__r.Minimum_Weight_for_Discount__c)||ISBLANK(Ship_To_Account_RelatedAccount__r.Ship_To__r.Minimum_Weight_for_Discount__c)||ISNULL(text(Ship_To_Account_RelatedAccount__r.Ship_To__r.Minimum_Weight_for_Discount_Unit__c))||ISBLANK(text(Ship_To_Account_RelatedAccount__r.Ship_To__r.Minimum_Weight_for_Discount_Unit__c))||ISNULL(Ship_To_Account_RelatedAccount__r.Ship_To__r.Discount_on_Weight__c)||ISBLANK(Ship_To_Account_RelatedAccount__r.Ship_To__r.Discount_on_Weight__c),IF(ISNULL(Account.Minimum_Weight_for_Discount__c)||ISBLANK(Account.Minimum_Weight_for_Discount__c)||ISNULL(text(Account.Minimum_Weight_for_Discount_Unit__c))||ISBLANK(text(Account.Minimum_Weight_for_Discount_Unit__c))||ISNULL(Account.Discount_on_Weight__c)||ISBLANK(Account.Discount_on_Weight__c),value($Label.CS_Weight_Discount),Account.Minimum_Weight_for_Discount__c), Ship_To_Account_RelatedAccount__r.Ship_To__r.Minimum_Weight_for_Discount__c)</formula>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CS Account Weight for Discount Unit</fullName>
        <field>Account_Wt_for_Discount_Unit__c</field>
        <formula>IF(ISNULL(Ship_To_Account_RelatedAccount__r.Ship_To__r.Minimum_Weight_for_Discount__c)||ISBLANK(Ship_To_Account_RelatedAccount__r.Ship_To__r.Minimum_Weight_for_Discount__c)||ISNULL(text(Ship_To_Account_RelatedAccount__r.Ship_To__r.Minimum_Weight_for_Discount_Unit__c)) || ISBLANK(text(Ship_To_Account_RelatedAccount__r.Ship_To__r.Minimum_Weight_for_Discount_Unit__c))||ISNULL(Ship_To_Account_RelatedAccount__r.Ship_To__r.Discount_on_Weight__c) || ISBLANK(Ship_To_Account__r.Discount_on_Weight__c),IF(ISNULL(Account.Minimum_Weight_for_Discount__c)||ISBLANK(Account.Minimum_Weight_for_Discount__c)||ISNULL(text(Account.Minimum_Weight_for_Discount_Unit__c))||ISBLANK(text(Account.Minimum_Weight_for_Discount_Unit__c))||ISNULL(Account.Discount_on_Weight__c)||ISBLANK(Account.Discount_on_Weight__c),$Label.CS_Weight_Discount_Unit,text(Account.Minimum_Weight_for_Discount_Unit__c)), text(Ship_To_Account_RelatedAccount__r.Ship_To__r.Minimum_Weight_for_Discount_Unit__c))</formula>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CS Account Wt Discount</fullName>
        <field>Account_Wt_for_Discount__c</field>
        <formula>IF(ISNULL(Ship_To_Account_RelatedAccount__r.Ship_To__r.Minimum_Weight_for_Discount__c)||ISBLANK(Ship_To_Account_RelatedAccount__r.Ship_To__r.Minimum_Weight_for_Discount__c)||ISNULL(text(Ship_To_Account_RelatedAccount__r.Ship_To__r.Minimum_Weight_for_Discount_Unit__c))||ISBLANK(text(Ship_To_Account_RelatedAccount__r.Ship_To__r.Minimum_Weight_for_Discount_Unit__c))||ISNULL(Ship_To_Account_RelatedAccount__r.Ship_To__r.Discount_on_Weight__c)||ISBLANK(Ship_To_Account_RelatedAccount__r.Ship_To__r.Discount_on_Weight__c),IF(ISNULL(Account.Minimum_Weight_for_Discount__c)||ISBLANK(Account.Minimum_Weight_for_Discount__c)||ISNULL(text(Account.Minimum_Weight_for_Discount_Unit__c))||ISBLANK(text(Account.Minimum_Weight_for_Discount_Unit__c))||ISNULL(Account.Discount_on_Weight__c)||ISBLANK(Account.Discount_on_Weight__c),value($Label.CS_Weight_Discount),Account.Minimum_Weight_for_Discount__c), Ship_To_Account_RelatedAccount__r.Ship_To__r.Minimum_Weight_for_Discount__c)</formula>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CS Global Percent Weight Discount</fullName>
        <field>Global_Percent_Weight_Discount__c</field>
        <formula>value($Label.CS_Percent_Weight_Discount)</formula>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CS Global Weight for Discount</fullName>
        <field>Global_Weight_for_Discount__c</field>
        <formula>value($Label.CS_Weight_Discount)</formula>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CS Global Weight for Discount Unit</fullName>
        <field>Global_Weight_for_Discount_Unit__c</field>
        <formula>$Label.CS_Weight_Discount_Unit</formula>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Credit Check Status</fullName>
        <field>Credit_Check_Status__c</field>
        <literalValue>Not Approved</literalValue>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>UpdateOrderTotalPriceField</fullName>
        <description>Field Update for workflow UpdateOrderTotalPrice that updates Total Price field when Added Discount % field is changed.</description>
        <field>Total_Price__c</field>
        <formula>TotalAmount - (TotalAmount*  Added_Discount__c )</formula>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>CS Account Discount Order</fullName>
        <actions>
            <name>CS Account Percent Weight Discount</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>CS Account Weight for Discount</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>CS Account Weight for Discount Unit</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>CS Global Percent Weight Discount</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>CS Global Weight for Discount</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>CS Global Weight for Discount Unit</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Order.of_Products__c</field>
            <operation>equals</operation>
            <value>0</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>CS Update Special Instructions</fullName>
        <active>true</active>
        <formula>true</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CS_UpdateOrderTotalPrice</fullName>
        <actions>
            <name>UpdateOrderTotalPriceField</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Updates the Total Price field  with respect to Added Discount  for the same Order Amount.</description>
        <formula>ISCHANGED( Added_Discount__c ) &amp;&amp; CONTAINS(Record_Type_Name__c  , &apos;North&apos;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
