<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Generate Related Account Unique Key</fullName>
        <description>Generate Related Account Unique Key, on creation or update of a Account junction object record.</description>
        <field>Uniqe_Key__c</field>
        <formula>Ship_To__c + &quot;-&quot; +  Sold_To__c</formula>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>CS_Populate_Unique_Key_For_Related_Accounts</fullName>
        <actions>
            <name>Generate Related Account Unique Key</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Populate_Unique_Key_For_Related_Accounts</description>
        <formula>true</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
