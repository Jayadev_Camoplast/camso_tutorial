<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Release Approval - Approved</fullName>
        <description>When the release is approved, the status changes to &apos;Approved&apos;</description>
        <field>Approval__c</field>
        <literalValue>Approved</literalValue>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Release Approval - Rejected</fullName>
        <description>When the release is rejected, the status changes to &apos;Rejected&apos;</description>
        <field>Approval__c</field>
        <literalValue>Rejected</literalValue>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Release Approval - Submitted</fullName>
        <description>When the release is submitted for approval, the approval status changes to &apos;Submitted&apos;</description>
        <field>Approval__c</field>
        <literalValue>Submitted</literalValue>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
</Workflow>
