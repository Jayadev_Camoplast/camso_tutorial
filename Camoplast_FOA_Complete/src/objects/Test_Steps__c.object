<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <deploymentStatus>Deployed</deploymentStatus>
    <description>Business Description: This object is used to record the test steps which have been executed.

Each Test Step is a copy of a &quot;Test Template Step&quot; which has been set up by a Test Manager or other Test Script writer.

Tests are created by the system automatically when a user is added to the Testers Related list on a Test Template.</description>
    <enableActivities>true</enableActivities>
    <enableHistory>true</enableHistory>
    <enableReports>true</enableReports>
    <fields>
        <fullName>Action__c</fullName>
        <formula>Template_Step__r.Action__c</formula>
        <inlineHelpText>What a user needs to do, including point and click instructions where appropriate</inlineHelpText>
        <label>Action</label>
        <type>Text</type>
    </fields>
    <fields>
        <fullName>Comments__c</fullName>
        <inlineHelpText>Notes and Comments about the test from the tester. This should be populated if a test step is failed in any way</inlineHelpText>
        <label>Comments</label>
        <type>TextArea</type>
    </fields>
    <fields>
        <fullName>Completed_by__c</fullName>
        <formula>Test__r.Assigned_To__r.FirstName &amp;  &quot; &quot; &amp;Test__r.Assigned_To__r.LastName</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <inlineHelpText>The name of the user who is the &quot;Completed by&quot; at the test level</inlineHelpText>
        <label>Completed by</label>
        <type>Text</type>
    </fields>
    <fields>
        <fullName>Expected_Results__c</fullName>
        <formula>Template_Step__r.Expected_Result__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <inlineHelpText>The result or behaviour that is expected</inlineHelpText>
        <label>Expected Results</label>
        <type>Text</type>
    </fields>
    <fields>
        <fullName>Outcome__c</fullName>
        <inlineHelpText>What was the result of this step? If it Failed or had minor issues please note what these were.</inlineHelpText>
        <label>Outcome</label>
        <picklist>
            <picklistValues>
                <fullName>Pass</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Passed with issues</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Failed</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>TBC</fullName>
                <default>true</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>StepNum__c</fullName>
        <formula>Template_Step__r.Name</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <inlineHelpText>The step number for this step</inlineHelpText>
        <label>Step #</label>
        <type>Text</type>
    </fields>
    <fields>
        <fullName>Template_Step__c</fullName>
        <inlineHelpText>A link to the template step for this test. Action, Expected, Results, Tips etc are all calculated based on this link</inlineHelpText>
        <label>Template Step</label>
        <referenceTo>Test_Template_Steps__c</referenceTo>
        <relationshipName>Test_Step_Results</relationshipName>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Test__c</fullName>
        <label>Test</label>
        <referenceTo>Test__c</referenceTo>
        <relationshipName>Test_Steps</relationshipName>
        <relationshipOrder>0</relationshipOrder>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>true</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>Tips__c</fullName>
        <formula>Template_Step__r.Tips__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <inlineHelpText>Hints &amp; Tips for completing this step</inlineHelpText>
        <label>Tips</label>
        <type>Text</type>
    </fields>
    <label>Test Execution Step</label>
    <nameField>
        <label>Ref</label>
        <type>Text</type>
    </nameField>
    <pluralLabel>Test Execution Steps</pluralLabel>
    <searchLayouts/>
    <sharingModel>ControlledByParent</sharingModel>
</CustomObject>
