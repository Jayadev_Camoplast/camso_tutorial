<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <fields>
        <fullName>Account__c</fullName>
        <formula>Order.Account.Name</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Account</label>
        <type>Text</type>
    </fields>
    <fields>
        <fullName>Back_Order__c</fullName>
        <defaultValue>false</defaultValue>
        <label>Back Order</label>
        <trackHistory>true</trackHistory>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Bill_To_Contact__c</fullName>
        <formula>Order.BillToContact.FirstName + &apos; &apos; + Order.BillToContact.LastName</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Bill To Contact</label>
        <type>Text</type>
    </fields>
    <fields>
        <fullName>CS_Quote_ID__c</fullName>
        <formula>Order.QuoteId</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>CS_Quote_ID</label>
        <type>Text</type>
    </fields>
    <fields>
        <fullName>Create_PO__c</fullName>
        <defaultValue>false</defaultValue>
        <label>Create PO</label>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Customer_Price__c</fullName>
        <formula>ListPrice</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Customer Price</label>
        <precision>18</precision>
        <scale>2</scale>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Delivery_Date__c</fullName>
        <label>Delivery Date</label>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Discount_Reason_Values__c</fullName>
        <description>In Epicor, the Discount Reason code in every line item required for when discount % is applied to a line item</description>
        <inlineHelpText>In Epicor, the Discount Reason code in every line item required for when discount % is applied to a line item</inlineHelpText>
        <label>Discount Reason</label>
        <picklist>
            <picklistValues>
                <fullName>10KVBD</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>COMP</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>GOODGUY5</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>ONE</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>PROMO</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>SUB</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>TO</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>WILL</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>Discount_Reason__c</fullName>
        <label>Discount Reason</label>
        <length>10</length>
        <trackHistory>true</trackHistory>
        <type>Text</type>
    </fields>
    <fields>
        <fullName>Discount__c</fullName>
        <label>Discount %</label>
        <precision>8</precision>
        <scale>2</scale>
        <trackHistory>true</trackHistory>
        <type>Percent</type>
    </fields>
    <fields>
        <fullName>Discount_from_SAP__c</fullName>
        <defaultValue>0.00</defaultValue>
        <label>Discount Schema</label>
        <precision>5</precision>
        <scale>2</scale>
        <type>Percent</type>
    </fields>
    <fields>
        <fullName>Epicor_External_ID__c</fullName>
        <externalId>true</externalId>
        <label>Epicor External ID</label>
        <length>255</length>
        <type>Text</type>
    </fields>
    <fields>
        <fullName>Fitted_On_Site__c</fullName>
        <defaultValue>false</defaultValue>
        <label>Fitted On Site</label>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Global_PID__c</fullName>
        <formula>OrderHistory__r.Global_PID__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Global PID</label>
        <type>Text</type>
    </fields>
    <fields>
        <fullName>Item__c</fullName>
        <defaultValue>0</defaultValue>
        <label>Item</label>
        <precision>10</precision>
        <scale>0</scale>
        <type>Number</type>
    </fields>
    <fields>
        <fullName>Line_Number__c</fullName>
        <defaultValue>0</defaultValue>
        <description>In Epicor, Line number (internal use only)</description>
        <inlineHelpText>In Epicor, Line number (internal use only)</inlineHelpText>
        <label>Line Number</label>
        <precision>18</precision>
        <scale>0</scale>
        <type>Number</type>
    </fields>
    <fields>
        <fullName>ListPricexQuantity__c</fullName>
        <formula>ListPrice * Quantity</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>ListPricexQuantity</label>
        <precision>18</precision>
        <scale>2</scale>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>List_Price__c</fullName>
        <description>Lookup to list price to be able to add to Order Products related list.</description>
        <formula>ListPrice</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>List Price</label>
        <precision>18</precision>
        <scale>2</scale>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Location__c</fullName>
        <label>Location</label>
        <referenceTo>Location__c</referenceTo>
        <relationshipLabel>Order Products</relationshipLabel>
        <relationshipName>Order_Products</relationshipName>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Margin__c</fullName>
        <label>Margin %</label>
        <length>20</length>
        <trackHistory>true</trackHistory>
        <type>Text</type>
    </fields>
    <fields>
        <fullName>Notes__c</fullName>
        <label>Notes</label>
        <length>255</length>
        <type>Text</type>
    </fields>
    <fields>
        <fullName>OrderHistory__c</fullName>
        <label>Product</label>
        <referenceTo>Product2</referenceTo>
        <relationshipLabel>Order History</relationshipLabel>
        <relationshipName>Order_Products</relationshipName>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Order_Created_Date__c</fullName>
        <formula>Order.CreatedDate</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Created Date</label>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Order_Number__c</fullName>
        <formula>HYPERLINK(&quot;/&quot;&amp;  OrderId, Order.OrderNumber)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Order Number</label>
        <type>Text</type>
    </fields>
    <fields>
        <fullName>PC__c</fullName>
        <label>PC</label>
        <length>1</length>
        <type>Text</type>
    </fields>
    <fields>
        <fullName>Part_Type__c</fullName>
        <label>Part Type</label>
        <picklist>
            <picklistValues>
                <fullName>c</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>e</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>j</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>m</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>p</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>v</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>x</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>QC_Reason_Code__c</fullName>
        <description>In Epicor, the Quality Control Reason code</description>
        <inlineHelpText>In Epicor, the Quality Control Reason code</inlineHelpText>
        <label>QC Reason Code</label>
        <picklist>
            <picklistValues>
                <fullName>Assembly</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>BADPRESS</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>C01</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>C02</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>C03</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>C04</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>C05</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>C06</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>C07</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>C08</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>C09</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>C10</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>C11</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>C12</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>C13</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>C14</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>CC</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>CONV</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Conversion</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>DIS</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>DS</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>FC</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>IN</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Marketing</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>ME</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>MM</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>PC</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Receipt</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Resin/Moca</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>RV</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>TakeOffs</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>TestTires</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>TestTracks</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>TestWheels</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Trade</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Training</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>TRANS</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>URTHF</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>Quality_Control__c</fullName>
        <description>In Epicor, Quality control required flag (credit return only)
• Y= needs to qc action
• n = no qc action needed
• f = qc action finished</description>
        <inlineHelpText>In Epicor, Quality control required flag (credit return only)
• Y= needs to qc action
• n = no qc action needed
• f = qc action finished</inlineHelpText>
        <label>Quality Control</label>
        <picklist>
            <picklistValues>
                <fullName>Y</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>n</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>f</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>Quantity__c</fullName>
        <description>To restrict user from entering Decimal Values for quantity</description>
        <label>Quantity</label>
        <precision>16</precision>
        <scale>0</scale>
        <type>Number</type>
    </fields>
    <fields>
        <fullName>Sales_Unit__c</fullName>
        <label>Sales Unit</label>
        <picklist>
            <picklistValues>
                <fullName>NUM</fullName>
                <default>true</default>
            </picklistValues>
            <picklistValues>
                <fullName>EA</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>Ship_To__c</fullName>
        <formula>Order.Ship_To_Account_RelatedAccount__r.Ship_To__r.Name</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Ship To</label>
        <type>Text</type>
    </fields>
    <fields>
        <fullName>Status__c</fullName>
        <description>Same values as Order.Status</description>
        <inlineHelpText>Same values as Order.Status</inlineHelpText>
        <label>Status</label>
        <picklist>
            <picklistValues>
                <fullName>a</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>b</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>c</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>e</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>h</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>l</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>m</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>n</fullName>
                <default>true</default>
            </picklistValues>
            <picklistValues>
                <fullName>p</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>q</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>r</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>s</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>t</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>v</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>x</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>SvcAgr__c</fullName>
        <defaultValue>false</defaultValue>
        <label>SvcAgr</label>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Tax_Code__c</fullName>
        <description>In Epicor, the Tax code</description>
        <inlineHelpText>In Epicor, the Tax code</inlineHelpText>
        <label>Tax Code</label>
        <picklist>
            <picklistValues>
                <fullName>AVATAX</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>NBTAX</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>NOTAX</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>Torque_Setting__c</fullName>
        <label>Torque Setting</label>
        <picklist>
            <picklistValues>
                <fullName>TBD</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>Total_Price__c</fullName>
        <formula>IF(NOT(ISBLANK( Discount__c )),  ((UnitPrice  *  Quantity )  - (UnitPrice  *  Quantity  * Discount__c  ))  , UnitPrice  *  Quantity)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Total (Discounted) Price</label>
        <precision>18</precision>
        <scale>2</scale>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Total_Tax__c</fullName>
        <defaultValue>0</defaultValue>
        <description>In Epicor, Total amount of tax for this line item</description>
        <inlineHelpText>In Epicor, Total amount of tax for this line item</inlineHelpText>
        <label>Total Tax</label>
        <precision>18</precision>
        <scale>2</scale>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Tyre_Condition__c</fullName>
        <label>Tyre Condition</label>
        <picklist>
            <picklistValues>
                <fullName>TBD</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>User_Count__c</fullName>
        <label>User Count</label>
        <precision>12</precision>
        <scale>2</scale>
        <type>Number</type>
    </fields>
    <fields>
        <fullName>Void__c</fullName>
        <defaultValue>false</defaultValue>
        <description>For Epicor sync</description>
        <inlineHelpText>For Epicor sync</inlineHelpText>
        <label>Void</label>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Volume__c</fullName>
        <formula>PricebookEntry.Product2.Volume__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Volume</label>
        <precision>18</precision>
        <scale>2</scale>
        <type>Number</type>
    </fields>
    <fields>
        <fullName>Weight__c</fullName>
        <formula>PricebookEntry.Product2.Weight__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Weight</label>
        <type>Text</type>
    </fields>
    <fields>
        <fullName>Wex__c</fullName>
        <defaultValue>false</defaultValue>
        <label>Wex</label>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Wheel_Position__c</fullName>
        <label>Wheel Position</label>
        <picklist>
            <picklistValues>
                <fullName>Front</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Rear</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>OSF</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>NSF</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>OSR</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>NSR</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>OSF Outer</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>OSF Inner</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>NSF Outer</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>NSF Inner</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Single Steer</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>Wt_Vol_Ratio__c</fullName>
        <label>Net Weight/Volume</label>
        <length>50</length>
        <type>Text</type>
    </fields>
    <validationRules>
        <fullName>Dicount_Reason_Required</fullName>
        <active>true</active>
        <errorConditionFormula>AND(ISBLANK(TEXT(Discount_Reason_Values__c)),AND(Discount__c!= 0))</errorConditionFormula>
        <errorMessage>Make sure to enter a Discount Reason for discounted products.</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>Discount_between_0_and_100</fullName>
        <active>true</active>
        <errorConditionFormula>OR( ( Discount__c  &lt; 0), ( Discount__c  &gt; 100))</errorConditionFormula>
        <errorDisplayField>Discount__c</errorDisplayField>
        <errorMessage>Discount can not be negative or greater than 100%.</errorMessage>
    </validationRules>
    <webLinks>
        <fullName>Verify_Stock</fullName>
        <availability>online</availability>
        <displayType>massActionButton</displayType>
        <linkType>javascript</linkType>
        <masterLabel>Verify Stock</masterLabel>
        <openType>onClickJavaScript</openType>
        <protected>false</protected>
        <url>window.open(&apos;apex/DTT_Inventory_Stock&apos;, &apos;&apos;, &apos;top=200, left=200, width=250, height=120&apos;);</url>
    </webLinks>
</CustomObject>
