<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <deploymentStatus>Deployed</deploymentStatus>
    <description>Business Description: Used to record meetings, including: minutes, attendees, outputs and links to risks and issues, decisions and questions.</description>
    <enableActivities>true</enableActivities>
    <enableHistory>true</enableHistory>
    <enableReports>true</enableReports>
    <fields>
        <fullName>Agenda__c</fullName>
        <label>Agenda</label>
        <trackHistory>true</trackHistory>
        <type>TextArea</type>
    </fields>
    <fields>
        <fullName>Attendance__c</fullName>
        <defaultValue>$User.FirstName &amp; &quot; &quot; &amp;  $User.LastName &amp;&quot;, &quot;</defaultValue>
        <description>People who attended the meeting.</description>
        <inlineHelpText>The people who attended this meeting in person or by phone. 

NB: Ensure you use same &quot;Firstname Lastname&quot; as listed in Tracker for all attendees to ensure this meeting appears in their list view &quot;Meetings I attended&quot;</inlineHelpText>
        <label>Attendance</label>
        <trackHistory>true</trackHistory>
        <type>TextArea</type>
    </fields>
    <fields>
        <fullName>End__c</fullName>
        <label>End</label>
        <trackHistory>true</trackHistory>
        <type>DateTime</type>
    </fields>
    <fields>
        <fullName>Key_Output__c</fullName>
        <label>Key Output</label>
        <length>32000</length>
        <trackHistory>true</trackHistory>
        <type>Html</type>
        <visibleLines>25</visibleLines>
    </fields>
    <fields>
        <fullName>Location__c</fullName>
        <label>Location</label>
        <length>255</length>
        <trackHistory>true</trackHistory>
        <type>Text</type>
    </fields>
    <fields>
        <fullName>Non_Attendance__c</fullName>
        <label>Non Attendance</label>
        <trackHistory>true</trackHistory>
        <type>TextArea</type>
    </fields>
    <fields>
        <fullName>Outcome_Summary__c</fullName>
        <description>Summary of the outcome of the meeting</description>
        <label>Outcome Summary</label>
        <trackHistory>true</trackHistory>
        <type>TextArea</type>
    </fields>
    <fields>
        <fullName>Project__c</fullName>
        <label>Project</label>
        <referenceTo>Project__c</referenceTo>
        <relationshipLabel>Meetings</relationshipLabel>
        <relationshipName>Meetings</relationshipName>
        <trackHistory>true</trackHistory>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Purpose__c</fullName>
        <label>Purpose</label>
        <trackHistory>true</trackHistory>
        <type>TextArea</type>
    </fields>
    <fields>
        <fullName>Sprint__c</fullName>
        <label>Sprint</label>
        <referenceTo>Sprint__c</referenceTo>
        <relationshipLabel>Meetings</relationshipLabel>
        <relationshipName>Meetings</relationshipName>
        <trackHistory>true</trackHistory>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Start__c</fullName>
        <label>Start</label>
        <trackHistory>true</trackHistory>
        <type>DateTime</type>
    </fields>
    <fields>
        <fullName>Type__c</fullName>
        <label>Type</label>
        <picklist>
            <picklistValues>
                <fullName>Requirements Meeting</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Design Meeting</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Workshop</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Sprint Planning</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Sprint Review</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Sprint Retrospective</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Status Meeting</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Steering Committee</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Team Building</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Other</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Leadership Team</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Developer Demonstration</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Project Integration</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <trackHistory>true</trackHistory>
        <type>Picklist</type>
    </fields>
    <label>Meeting</label>
    <nameField>
        <label>Meeting Name</label>
        <trackHistory>true</trackHistory>
        <type>Text</type>
    </nameField>
    <pluralLabel>Meetings</pluralLabel>
    <searchLayouts>
        <customTabListAdditionalFields>Project__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Start__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>End__c</customTabListAdditionalFields>
        <excludedStandardButtons>ChangeOwner</excludedStandardButtons>
        <excludedStandardButtons>Accept</excludedStandardButtons>
        <lookupDialogsAdditionalFields>Project__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Start__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>End__c</lookupDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>Project__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>Start__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>End__c</lookupPhoneDialogsAdditionalFields>
        <searchFilterFields>NAME</searchFilterFields>
        <searchFilterFields>Project__c</searchFilterFields>
        <searchFilterFields>Start__c</searchFilterFields>
        <searchFilterFields>End__c</searchFilterFields>
        <searchResultsAdditionalFields>Project__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Start__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>End__c</searchResultsAdditionalFields>
    </searchLayouts>
    <sharingModel>ReadWrite</sharingModel>
</CustomObject>
